# AutoImage

Automation for configuring an [OpenBSD](http://www.openbsd.org/) build machine and building [resflash](https://gitlab.com/bconway/resflash) images.

Resflash is a terrific tool for creating resilient OpenBSD images (see https://gitlab.com/bconway/resflash).

This is my automation to help build OpenBSD images using Resflash and the associated build_premade.sh
(see https://gitlab.com/bconway/resflash-tools) for my routers.
It suits my needs. Your mileage may vary. Merge requests will be considered.

[[_TOC_]]

## Components

### Machine configuration script

The **config_machine.sh** script is used to configure a new machine for the
automation.

#### Steps

The machine configuration script executes the following configuration steps:
1. Sets up file systems for building images (see [Build machine directory structure](#build-machine-directory-structure) below)
1. Patches machine operating system (runs syspatch twice)
1. Installs additional AutoImage scripts from archive or source control
1. Installs config from archive or source control (see **-c** and **-g**)
1. Downloads and installs, and patches resflash
1. Configures resflash.conf if requested (see **-r** and **-config_resflash**)
1. Downloads and patches resflash-tools/build_premade.sh to **build_premade_auto.sh**
1. Installs sources into /usr/src
   - **Existing sources, if any, are removed**
   - Sources are extracted from /tmp/{src,sys}.tar.gz if found, otherwise source files are downloaded from `/etc/installurl`
1. Attempts to patch sources (runs the **patch_source.sh** script)
1. Starts building release sets if requested (runs **build_release.sh** script) (see **-b** and **-build_release**)
   - Building release sets can take a long time
   - On my machines, building release sets takes around 4-5 hours (longer for i386)

#### Build machine directory structure

This is the directory structure created by [config_machine.sh](#machine-configuration-script) and used by AutoImage:
```
/
└───imaging/                        [(ROOT_DIR)]
    ├───base/                       [build_premade.sh and build_premade_auto.sh extract sets in base (RESFLASH_BASEDIR)]
    ├───build/                      [Build directory (BUILD_DIR)]
    │   ├───dst/                    [Build install directory (BUILD_DEST_DIR)]
    │   └───rel/                    [Build release sets directory (RESFLASH_RELDIR)]
    ├───config/                     [Configuration file root (CONFIG_DIR) see [Configuration directory structure]]
    ├───images/                     [Resflash image directory (IMAGE_DIR)]
    │   ├───<image_name>            [Config name directory for first config]
    │   │   └───premade/            [build_premade.sh and build_premade_auto.sh image directory]
    │   │       ├───install/        [Install images (*.img.gz)]
    │   │       └───upgrade/        [Upgrade images (*.fs.gz)]
    │   └───<image_name2>           [Config name directory for second config if any]
    │       └───premade/            [build_premade.sh and build_premade_auto.sh image directory]
    │           ├───install/        [Install images (*.img.gz)]
    │           └───upgrade/        [Upgrade images (*.fs.gz)]
    ├───resflash/                   [Resflash script directory (RESFLASH_DIR)]
    └───scripts/                    [AutoImage scripts directory (SCRIPT_DIR)]
        ├───build_premade.sh.orig   [Resflash tools build_premade.sh script]
        ├───build_premade.sh.patch  [Patch file used to modify build_premade.sh into build_premade_auto.sh]
        ├───build_premade_auto.sh   [AutoImage image build script]
        ├───build_release.sh        [Script to build release sets]
        ├───config_host.sh          [Standalone host configuration script (analog of config_image.sh)]
        ├───config_image.sh         [AutoImage image configuration script]
        ├───download_config.sh      [Down load configuration files from repository and extract to /imaging/config]
        ├───make_image.sh           [AutoImage image build and configuration script]
        ├───patch_source.sh         [Script to automate source installation and patch application]
        ├───upgrade.sh.patch        [Patch file used to modify /imaging/resflash/host/upgrade.sh]
        ├───upgrade_host.sh         [Script to automate image build and configuration and remote host upgrade]
        └───upgrade_remote_host.sh  [Script to automate upgrade of image on remote host]
```

#### Usage

Run as `root`.

```shell
/tmp/config_machine.sh [-b|--build_release] [-r|--config_resflash] [-c config_archive_path] [-g|--gitlab_token]
```
Options:

|Position|Argument|Description|
|--------|--------|-----------|
||-b\|--build_release|Switch that starts building the release sets at the end of the machine configuration process|
||-r\|--config_resflash|Switch that configures resflash with save_etc & save_etc (Change DEFAULT_RESFLASH_SAVE_ETC or DEFAULT_RESFLASH_SAVE_VAR in script to customize)|
||-c config_archive_path|Specifies the path (URL or local path) to the archive file containing the config files, assumes local path of /tmp/config_*.tar.gz if not specified|
||-g\|--gitlab_token|Switch to turn on the use of GitLab tokens when retrieving the config file archive, only valid with -c, token will be requested on console. Currently, only supports GitLab API tokens|
||-n\|--no_jobs|Optional switch to disable multiple jobs in build process, only applicable with build_release switch|

Example:
```shell
/tmp/config_machine.sh -build_release -config_resflash -gitlab_token -c https://gitlab.com/api/v4/projects/<project_id>/repository/archive.tar.gz
```

### Image build script

The **build_premade_auto.sh** script is a modified version of [build_premade.sh](https://gitlab.com/bconway/resflash-tools/raw/master/build_premade.sh) in [resflash-tools](https://gitlab.com/bconway/resflash-tools/) used to automate the building and configuration of resflash images. It calls the image configuration script (**config_image.sh**) during the build_premade process. Note: The **build_premade_auto.sh** script must be run as `root` in order to extract the sets correctly.

See the **[make_image.sh](#make-image-script)** script for a simple way to invoke the **build_premade_auto.sh** script.

#### Steps

The script executes the following steps:
1. Clean old images
1. Make base dir
1. Extract sets
1. Build VGA images if requested
1. Build com0/serial images
1. Configure images (if **config_image.sh** is not found images will not be configured)
1. Compress images
1. Calculate image checksums

#### Usage

```shell
mkdir -p /imaging/images/config_name
cd /imaging/images/config_name
../../scripts/build_premade_auto.sh resflash_dir config_name [-b|--build_vga]
```
Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|resflash_dir|This is the path to the resflash scripts (always _/imaging/resflash_)|
|2|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
||-b\|--build_vga|Build VGA images. Default is to not build VGA images|

Note: `--build_vga` does not seem to be needed for Protectli machines even though they have VGA (HDMI) output

Example:
```shell
cd /imaging/images/<config_name>
../scripts/build_premade_auto.sh /imaging/resflash example
```

### Build release script

The **build_release.sh** automates the building of release sets.

#### Usage

The script may be called during the initial machine configuration in
**config_machine.sh** if requested with the **-b** or **-build_release**
options. It can also be used in a standalone manner to build new release sets.
By default the script attempts to build the release with multiple jobs based
on the number of CPUs online and physical memory to speed up the build process.
This can be disabled using the `--no_jobs` switch to run the build with a
single job (not recommended, unless multiple jobs are causing problems).

|Position|Argument|Description|
|--------|--------|-----------|
||-n\|--no_jobs|Optional switch to disable multiple jobs in build process and run the build with a single job|

Example:
```shell
/imaging/scripts/patch_source.sh && /imaging/scripts/build_release.sh
```

### Host configuration script

The **config_host.sh** is a standalone script used to automate host configuration
by applying configuration settings based on the specified configuration files stored in
source control (or an archive file).
The script is a standalone version of [config_image.sh](#image-configuration-script).
Unlike [config_image.sh](#image-configuration-script), **config_host.sh**
does not generate an image, but directly configures the host it is running
on.

#### Steps

The script executes the following configuration steps:
1. Gather user passwords, private keys, IPSec PSKs, and additional passwords (**ADMIN_USER**, **ADMIN_ID**, **IPSEC_PSK_NAME**, **PASSWORD_PROMPT**, **PASSWORD_FILE**)
1. Add admin users (**ADMIN_USER**, **ADMIN_ID**)
1. Add no-password users (**NO_PW_USER**, **NO_PW_USER_ID**, **NO_PW_USER_HOME**, **NO_PW_USER_SHELL**, **NO_PW_USER_DESC**)
1. Change root user name if requested (**CHANGE_ROOT_NAME_TO_HOSTNAME**)
1. Set root user to not clear screen after exiting `man`
1. Set vi tab stops to 4 for root
1. Set root password
1. Update password database
1. Set no-password user groups (**NO_PW_USER_GROUPS**)
1. Set admin users to not clear screen after exiting `man`
1. Set vi tab stops to 4 for admin users
1. Add admin user public keys (authorized_keys)
1. Copy files/doas.conf or copy from /etc/examples
1. Copy/create ssh_banner, sshd_config (**SSH_BANNER**, **SSHD_CONFIG**)
1. Create /etc/mygate (**MY_GATE**)
1. Create /etc/myname (**MY_NAME**)
1. Copy files/hostname.\*\
   Rename hostname.\* files with correct interface name if **INTERFACE** is defined\
   Replace interface place-holders if **INTERFACE** is defined
1. Copy files/dhcpd.conf
1. Copy files/hosts or create default hosts file with localhost if it doesn't already exist
1. Copy/create /etc/isakmpd/isakmpd.policy file (**ISAKMPD_POLICY**)
1. Copy IPSec config files (**IPSEC_FILE_PATTERN**)\
   Replace PSK place-holders if **IPSEC_PSK_NAME** defined
1. Copy pf.conf\
   Replace interface place-holders if **INTERFACE** is defined
1. Copy/create /etc/ntpd.conf (**NTP_CONF**)
1. Install additional packages (**ADDL_PACKAGES**)
1. Copy/create /etc/rc.conf.local (**RC_CONF_LOCAL**)
1. Copy/create /etc/rc.local (**RC_LOCAL**)
1. Copy/create /etc/sysctl.conf (**SYSCTL_ENABLE_IPV4_FORWARDING**, **SYSCTL_ENABLE_IPV6_FORWARDING**)
1. Copy/create unbound configuration (**CONFIGURE_UNBOUND**, **UNBOUND_CONF**)\
   Replace interface place-holders if **INTERFACE** is defined in unbound.conf\
   Retrieves current root hints file\
   Runs unbound-anchor to set the root trust anchor for DNSSEC validation\
   You are responsible for enabling unbound in /etc/rc.conf.local (**RC_CONF_LOCAL**),
   configuring resolv.conf, and any necessary changes to dhcpd.conf (**MISC_ETC_FILE**)
1. Copy additional miscellaneous files to /etc (**MISC_ETC_FILE**, **MISC_ETC_PERMS**, **MISC_ETC_OWNER**)
1. Copy additional miscellaneous files to other locations (**MISC_MISC_FILE**, **MISC_MISC_PERMS**, **MISC_MISC_OWNER**)
1. Copy additional script files to SCRIPT_PATH (**SCRIPT_FILE**, **SCRIPT_PERMS**, **SCRIPT_OWNER**, **SCRIPT_PATH**)
1. Add additional root crontab entries (**ADDL_CRON_PATH**)
1. Replace passwords in files if specified (**PASSWORD_FILE**, **PASSWORD_PROMPT**)
1. Set timezone if specified (**TIMEZONE_PATH**)
1. Run post-install scripts (**POST_INSTALL_ACTION**, **POST_INSTALL_SCRIPT**, **POST_INSTALL_DESC**)

#### Host configuration file

The file format is identical to the Image configuration file format.
See [Image configuration file](image-configuration-file) below for details.
The main difference is that the config source (either source control or
an archive file) contains the configuration for only the host being configured.

#### Usage

The **config_host.sh** script is called from the command line **on** **the** **host** **to** **be** **configured**.
Copy the script to the host to be configured (e.g., /tmp) and mark the script as executable.
Note: Config archive path URL should be to the config directory, e.g.,
https://gitlab.com/<account_name>/<project_name>/-/archive/master/<project_name>-master.tar.gz?path=config/<config_name> \
Run as `root`.

```shell
/tmp/config_host.sh config_name [-c config_archive_path] [-g|--gitlab_token]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
||-c config_archive_path|Optional path to the config files (URL or local path) assumes local path of /tmp/*config-<config_name>*.tar.gz if not specified<br />URL format can specify the entire archive, but can also specify the <config_name> folder, e.g.,<br />https://gitlab.com/<account_name>/<project_name>/-/archive/master/<project_name>-master.tar.gz?path=config/<config_name>|
||-g\|--gitlab_token|Optional switch to force use of GitLab token when retrieving config files (token will be requested on console). Currently only supports GitLab API tokens|

### Image configuration script

The **config_image.sh** is used to automate image configuration by applying
configuration settings based on the specified configuration files stored in the
build machine's file system. It is intended to be called automatically from the
image build script **[build_premade_auto.sh](#image-build-script)**.

#### Steps

The script executes the following configuration steps:
1. Gather user passwords, private keys, IPSec PSKs, and additional passwords (**ADMIN_USER**, **ADMIN_ID**, **IPSEC_PSK_NAME**, **PASSWORD_PROMPT**, **PASSWORD_FILE**)
1. Get root hints for Unbound (**CONFIGURE_UNBOUND**)
1. Loop over images (install and upgrade), mounting and configuring
1. Add admin users (**ADMIN_USER**, **ADMIN_ID**)
1. Add no-password users (**NO_PW_USER**, **NO_PW_USER_ID**, **NO_PW_USER_HOME**, **NO_PW_USER_SHELL**, **NO_PW_USER_DESC**)
1. Change root user name if requested (**CHANGE_ROOT_NAME_TO_HOSTNAME**)
1. Set root user to not clear screen after exiting `man`
1. Set vi tab stops to 4 for root
1. Set root password
1. Update password database
1. Set no-password user groups (**NO_PW_USER_GROUPS**)
1. Set admin users to not clear screen after exiting `man`
1. Set vi tab stops to 4 for admin users
1. Add admin user public keys (authorized_keys)
1. Copy files/doas.conf or copy from /etc/examples
1. Add ro/rw scripts if requested (**ADD_RO_RW_SCRIPTS**)\
   Modify /etc/weekly with ro/rw if requested (**ADD_RO_RW_TO_WEEKLY**), only
   in conjunction with **ADD_RO_RW_SCRIPTS**
1. Copy/create ssh_banner, sshd_config (**SSH_BANNER**, **SSHD_CONFIG**)
1. Create /etc/mygate (**MY_GATE**)
1. Create /etc/myname (**MY_NAME**)
1. Copy files/hostname.\*\
   Rename hostname.\* files with correct interface name if **INTERFACE** is defined\
   Replace interface place-holders if **INTERFACE** is defined
1. Copy files/dhcpd.conf
1. Copy files/hosts or create default hosts file with localhost
1. Copy/create /etc/isakmpd/isakmpd.policy file (**ISAKMPD_POLICY**)
1. Copy IPSec config files (**IPSEC_FILE_PATTERN**)\
   Replace PSK place-holders if **IPSEC_PSK_NAME** defined
1. Copy pf.conf\
   Replace interface place-holders if **INTERFACE** is defined
1. Copy/create /etc/ntpd.conf (**NTP_CONF**)
1. Copy/create /etc/rc.conf.local (**RC_CONF_LOCAL**)
1. Copy/create /etc/rc.local (**RC_LOCAL**)
1. Copy/create /etc/sysctl.conf (**SYSCTL_ENABLE_IPV4_FORWARDING**, **SYSCTL_ENABLE_IPV6_FORWARDING**)
1. Copy/create unbound configuration (**CONFIGURE_UNBOUND**, **UNBOUND_CONF**)\
   Replace interface place-holders if **INTERFACE** is defined in unbound.conf\
   Retrieves current root hints file\
   Runs unbound-anchor to set the root trust anchor for DNSSEC validation\
   You are responsible for enabling unbound in /etc/rc.conf.local (**RC_CONF_LOCAL**),
   configuring resolv.conf, and any necessary changes to dhcpd.conf (**MISC_ETC_FILE**)
1. Copy additional miscellaneous files to /etc (**MISC_ETC_FILE**, **MISC_ETC_PERMS**)
1. Copy additional script files to SCRIPT_PATH (**SCRIPT_FILE**, **SCRIPT_PERMS**, **SCRIPT_PATH**)
1. Copy additional miscellaneous files to /etc (**MISC_ETC_FILE**, **MISC_ETC_PERMS**, **MISC_ETC_OWNER**)
1. Copy additional miscellaneous files to other locations (**MISC_MISC_FILE**, **MISC_MISC_PERMS**, **MISC_MISC_OWNER**)
1. Copy additional script files to SCRIPT_PATH (**SCRIPT_FILE**, **SCRIPT_PERMS**, **SCRIPT_OWNER**, **SCRIPT_PATH**)
1. Add additional root crontab entries (**ADDL_CRON_PATH**)
1. Replace passwords in files if specified (**PASSWORD_FILE**, **PASSWORD_PROMPT**)
1. Set timezone if specified (**TIMEZONE_PATH**)

Note: Post-install scripts are **ignored** by **config_image.sh** (**POST_INSTALL_ACTION**, **POST_INSTALL_SCRIPT**, **POST_INSTALL_DESC**)

#### Image configuration file

- Defines most of the settings that control the configuration of the image (see steps above)
- Path: /imaging/config/<config_name>/**<config_name>.conf**
- Most configuration settings are optional (as are the steps they control). Comment out the unneeded settings.
- See [example.conf](config/example/example.conf) for examples and more information

#### Configuration directory structure

```
imaging/
└───config/
    ├───example/                [Example config root]
    │   ├───example.conf        [Image configuration file for example config]
    │   └───files/              [Files to be copied to the image]
    │       ├───hostname.0
    │       ├───hostname.1
    │       ├───ipsec.conf
    │       ├───pf.conf
    │       └───rc.conf.local
    ├───example2/               [Example2 config root]
    │   ├───example2.conf       [Image configuration file for example2 config]
    │   ├───files/              [Files to be copied to the image]
    │   │   └───hostname.0
    │   └───patches/            [Optional local patches to apply in [Patch script](#patch-script)]
    │       ├───local1.patch
    │       └───local2.patch
    ...
```

#### Usage

The **config_image.sh** script is usually called automatically from **[build_premade_auto.sh](#image-build-script)**
```shell
/imaging/scripts/config_image.sh config_name image_path
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
|2|image_path|Path to the image (.fs or .img) to be configured (e.g., _/imaging/images/resflash-amd64-1906MB-com0_115200-20180303_1534.img_)|


### Download config script

The **download_config.sh** script is a standalone script that can be used to update the config files from an archive or source control.
The config files are extracted into the correct location.
The [config_machine.sh](#machine-configuration-script) script will perform this
process initially, if it can find the scripts.
Use this script to update the configuration from an archive or source control.

#### Steps

The script executes the following steps:
1. Install wget if not already installed (used for download)
1. Delete old config directory (/imaging/config.old) if any
1. Rename current config directory (/imaging/config) with .old suffix (/imaging/config.old)
1. Download archive if remote archive
1. Extract archive into config directory (/imaging/config)

#### Usage

```shell
/imaging/scripts/download_config.sh [-c config_archive_path [-g|--gitlab_token]]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
||-c config_archive_path|Specifies the path (URL or local path) to the archive file containing the config files, assumes local path of /tmp/config_*.tar.gz if not specified|
||-g\|--gitlab_token|Switch to turn on the use of GitLab tokens when retrieving the config file archive, only valid with -c, token will be requested on console. Currently supports only GitLab API tokens|

Example:
```shell
/imaging/scripts/download_config.sh -gitlab_token -c https://gitlab.com/api/v4/projects/<project_id>/repository/archive.tar.gz
```

### Make Image script

The **make_image.sh** script simplifies the setup, build, and configuration of images after the release has been built (or rebuilt).

#### Steps

The script executes the following steps:
1. Create the config-specific image directory (e.g., /imaging/images/config_name) if it doesn't already exist
1. Change to the config-specific image directory
1. Starts the **[build_premade_auto.sh](#image-build-script)** script with appropriate parameters for config_name
1. Displays the **file-system** checksum hash if successful
1. Change back to starting directory

#### Usage

The **make_image.sh** script is called from the command-line:
```shell
/imaging/scripts/make_image.sh config_name [-b|--build_vga]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
||-b\|--build_vga|Build VGA images. Default is to not build VGA images|

Note: `--build_vga` does not seem to be needed for Protectli machines even though they have VGA (HDMI) output

### Patch script

The **patch_source.sh** script attempts to re-install source files and patch
them, optionally applying local config-specific patches.
The script is called during the initial machine configuration in
**config_machine.sh**.

#### Steps

The script executes the following steps:
1. Install wget if not already installed (used to download source and patches)
1. Fetch source archives (SHA256.sig, src.tar.gz, & sys.tar.gz) if not found
   in /tmp
1. **Delete existing source (/usr/src/\*) if any**
1. Extract source into /usr/src
1. Apply local patches, if any found in config-specific patch folder
   Local patches are stored in /imaging/config/config_name/patches
1. Fetch source patches
1. Extract source patches
1. Apply source patches

#### Usage

The script is called during the initial machine configuration in
**config_machine.sh**. It can also be used in a standalone manner to patch
before building a new release.
```shell
/imaging/scripts/patch_source.sh [config_name]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|_Optional_ name of the configuration folder/directory to use (e.g., _example_) if applying local patches specifically for config_name|

Example patch with local patches for 'k2' configuration:
```shell
/imaging/scripts/patch_source.sh k2
```

Example patch and rebuild _without_ applying any local patches:
```shell
/imaging/scripts/patch_source.sh && /imaging/scripts/build_release.sh
```

### Upgrade Host script

The **upgrade_host.sh** fully automates image build, image configuration, **and**
upgrading the target host with the newly configured image.
The **[make_image.sh](#make-image-script)** and the
**[upgrade_remote_host.sh](#upgrade-remote-host-script)** are called.

#### Steps

The script executes the following steps:
1. Change file mode on SSH key(s) if not already `0600` (SSH will fail if keys
   are not `0600`)
1. Gather user passwords, private keys for remote access
1. Call **[make_image.sh](#make-image-script)** to build and configure a new
   image
1. Call **[upgrade_remote_host.sh](#upgrade-remote-host-script)** to upgrade
   the image on the remote host

#### Usage

```shell
/imaging/scripts/upgrade_host.sh config_name host_name_or_ip ssh_key_path [-b|--build_vga] [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
|2|host_name_or_ip|Name or IP address of remote host where image will be upgraded|
|3|ssh_key_path|Path to SSH key used to access the remote host|
||-b\|--build_vga|Build VGA images. Default is to not build VGA images|
||-k\|--keep_known_hosts|Keep known hosts configuration, default is to remove known hosts entries for host_name_or_ip|
||-n\|--newkey ssh_key_path|Optional path to new SSH key to use post-upgrade, only makes sense with -reboot when changing SSH keys|
||-p\|--port ssh_port|Optional SSH port, default is to use the SSH port found in /etc/services|
||-r\|--reboot|Reboot the remote host if the image upgrade was successful and the remote image hash matches the local image hash, default is to NOT reboot|

Example:

```shell
/imaging/scripts/upgrade_host.sh example example.domain.local /path/to/ssh_key.ssh -port 22227 -reboot
```

### Upgrade Remote Host script

The **upgrade_remote_host.sh** automates the image upgrade process on the
remote host and attempts to verify the process succeeded and that the host is
responding again.
The script is normally called by **[upgrade_host.sh](#upgrade-host-script)**.
It can also be used as a standalone script.

When called by another script, remote access credentials are expected on stdin, separated by tab characters (\t):\
`REMOTE_USER_NAME\tREMOTE_USER_PASS\tSSH_KEY_PASS\tNEW_SSH_KEY_PASS`
- REMOTE_USER_NAME = user name used to login via SSH to target host
- REMOTE_USER_PASS = Password for REMOTE_USER_NAME used to run doas commands
- SSH_KEY_PASS = Passphrase for ssh_key_path (used to add key to ssh-agent)
- NEW_SSH_KEY_PASS = Passphrase for new ssh_key_path (used to add key to ssh-agent), blank/empty if none

#### Dependencies

- `expect` is used to handle some remote tasks

#### Steps

The script executes the following steps:
1. Change file mode on SSH key(s) if not already 0600 (SSH will fail if keys
   are world-readable)
1. Gather user passwords, private keys for remote access
1. Temporarily update the doas.conf settings on the remote host to make it
   possible to run /resflash/upgrade.sh
1. Run /resflash/upgrade.sh on remote host using image found for config_name
   See [resflash instructions](https://gitlab.com/bconway/resflash#upgrades)
   for more information about /resflash/upgrade.sh
1. Verify upgrade hash against image hash
1. Reboot remote host if requested
   1. Wait for remote host to start responding again after reboot
   1. Verify SSH to remote host is responding after reboot

#### Usage

```shell
/imaging/scripts/upgrade_remote_host.sh config_name host_name_or_ip ssh_key_path [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]
```

Options:

|Position|Argument|Description|
|--------|--------|-----------|
|1|config_name|Name of the configuration folder/directory to use (e.g., _example_)|
|2|host_name_or_ip|Name or IP address of remote host where image will be upgraded|
|3|ssh_key_path|Path to SSH key used to access the remote host|
||-b\|--build_vga|Build VGA images. Default is to not build VGA images|
||-k\|--keep_known_hosts|Keep known hosts configuration, default is to remove known hosts entries for host_name_or_ip|
||-n\|--newkey ssh_key_path|Optional path to new SSH key to use post-upgrade, only makes sense with -reboot when changing SSH keys|
||-p\|--port ssh_port|Optional SSH port, default is to use the SSH port found in /etc/services|
||-r\|--reboot|Reboot the remote host if the image upgrade was successful and the remote image hash matches the local image hash, default is to NOT reboot|

## Script dependencies

The following are common entry points to the scripts and their dependencies.

### [config_machine.sh](#machine-configuration-script)
- [patch_source.sh](#patch-script)
- [build_release.sh](#build-release-script)

### [make_image.sh](#make-image-script)
- [build_premade_auto.sh](#image-build-script)
  - [config_image.sh](#image-configuration-script)

### [download_config](#download-config-script)
None

### [patch_source.sh](#patch-script)
None

### [build_release.sh](#build-release-script)
None

### [upgrade_host.sh](#upgrade-host-script)

- [make_image.sh](#make-image-script)
  - [build_premade_auto.sh](#image-build-script)
    - [config_image.sh](#image-configuration-script)
- [upgrade_remote_host.sh](#upgrade-remote-host-script)

### config_host.sh
None

## Getting started

### Configure the image build machine

Image Build Machine Specifications:
- 1-8 processors (I use 8 cores/1 socket)
- 10 GB (10240 MB) RAM (or at least 1 GB/processor plus 2 GB)
- NAT networking
- 30 GB disk space
- Remove sound card
- Add USB 3 (or whatever you may need to write images to other media)

1. Install OpenBSD (http://www.openbsd.org/)
   - No additional users are needed as all of the scripts are run as `root` on the image build machine

   - File system layout (edit auto layout, let installer set size of swap/b partition)

     |Partition|Size|Mount point|File system|Size|Mounted on|Notes|
     |---------|----|-----------|-----------|----|----------|-----:|
     |a|1g|/|/dev/wd0a|1.0G|/||
     |d|1g|/var|/dev/wd0d|1.0G|/var||
     |e|3g|/usr|/dev/wd0e|3.0G|/usr||
     |f|1g|/usr/X11R6|/dev/wd0f|1.0G|/usr/X11R6||
     |g|500m|/usr/local|/dev/wd0g|500M|/usr/local||
     |h|3g|/usr/src|/dev/wd0h|3.0G|/usr/src||
     |i|6g|/usr/obj|/dev/wd0i|6.0G|/usr/obj||
     |j|100m|/home|/dev/wd0j|100M|/home||
     |k|4g|/tmp|/dev/wd0k|4.0G|/tmp||
     |l|3g|/imaging|/dev/wd0l|3.0G|/imaging|# ROOT_DIR|
     |m|5.4g|/imaging/build|/dev/wd0m|5.4G|/imaging/build|Use all remaining space for this partition # BUILD_DIR|

     If the mount point is changed in partition `l` (and partition `m`) you will
     need to modify `ROOT_DIR` near the top of
     **[config_machine.sh](config_machine.sh)**

   - Install all sets except games (unless you require them)
     ```
     -g*
     ```

1. Copy **[config_machine.sh](config_machine.sh)** script to /tmp on machine

1. Set executable bit on the script
   ```shell
      chmod a+x /tmp/config_machine.sh
   ```

1. Optionally copy the following to /tmp:
   - SHA256.sig\
     OpenBSD source signature, will be downloaded from `/etc/installurl` if not
     found
   - src.tar.gz\
     OpenBSD source, will be downloaded from `/etc/installurl` if not found
   - sys.tar.gz\
     OpenBSD source, will be downloaded from `/etc/installurl` if not found
   - config_\*.tar.gz\
     Configuration files for images, can be downloaded from source control if a URL is specified in the **-c** command-line argument.
   - scripts_\*.tar.gz\
     Additional AutoImage scripts required to build images, will be downloaded from https://gitlab.com/gregwinterstein/autoimage if not found.
     These scripts are normally downloaded automatically from https://gitlab.com/gregwinterstein/autoimage.
     Including the additional AutoImage scripts in `/tmp` is only needed if alternate/modified versions of the scripts are required.

1. Run the **[config_machine.sh](#machine-configuration-script)** script to configure the machine, download additional scripts and config, and start building the release sets
   ```shell
      /tmp/config_machine.sh -build_release -config_resflash -gitlab_token -c https://gitlab.com/api/v4/projects/<project_id>/repository/archive.tar.gz
   ```

1. Configure resflash to save some files on reboot\
   This step only needs to be done once per build machine, and only if it was **not** done by/with [config_machine.sh](config_machine.sh)\
   Note: The saved files specified in resflash.conf will survive both reboots and **upgrades** of the imaged machines.
   Do not save files via resflash.conf that should change during upgrades.\
   Edit **/imaging/resflash/etc/resflash.conf** and make any necessary changes.\
   Example:
   ```shell
   save_etc='isakmpd/local.pub isakmpd/private/local.key'
   save_var='db/host.random db/dhcpd.leases'
   ```

1. Set up configuration files if they don't yet exist\
   See [Image configuration file](#image-configuration-file), [Configuration directory structure](#configuration-directory-structure), and [example.conf](config/example/example.conf) for examples and more information

### Patch and build release sets

This is initially triggered by the [config_machine.sh](#machine-configuration-script) script if started with `--build-release`.
To build new patched release sets run the following (see [Patch script](#patch-script) and [Build release script](#build-release-script) for caveats):
```shell
/imaging/scripts/patch_source.sh && /imaging/scripts/build_release.sh
```

### Build and configure initial resflash images

1. Run the **[make_image.sh](#make-image-script)** script to build and configure the resflash images. This takes about five minutes.
   ```shell
   /imaging/scripts/make_image.sh config_name [--build_vga]
   ```
   Example:
   ```shell
   /imaging/scripts/make_image.sh example
   ```
1. Copy install image to USB drive.\
   Install images are found in `/imaging/images/<config_name>/premade/install/`
   ```shell
   cp /imaging/images/<config_name>/premade/install/resflash-amd64-1906MB-com0_115200-20200913_0853.img.gz /mnt/usb
   ```
1. Boot **bsd.rd** (either from USB drive or existing installation)
   1. At the **boot>** prompt enter:
      ```shell
      boot bsd.rd
      ```
   1. Enter the shell:
      ```shell
      (I)nstall, (U)pgrade, (A)utoinstall or (S)hell? s
      ```

   1. Verify internal disk is **sd0** or whatever id it might be (for Protectli Vault, **Protectli 16GB**)
      ```shell
      dmesg | grep Protectli
      ```

   1. Create devices
      ```shell
      cd /dev
      sh MAKEDEV sd0 sd1 sd2
      ```

   1. Insert USB drive containing new image and mount (sd1a or sd2a, watch console messages)
      ```shell
      mount /dev/sd1a /mnt
      ```

   1. Unzip and copy the image from USB drive onto local disk (sd0) using dd:
      ```shell
      gunzip -c /mnt/resflash*.img.gz | dd of=/dev/rsd0c bs=1m
      umount /mnt
      reboot
      ```

#### For initial installation on removable media
With removable target media, such as CF cards or other removable drives,
the image can be installed directly without copying to a USB drive as above.

1. Unzip and copy using dd:
   ```shell
   gunzip -c /imaging/images/<config_name>/premade/install/resflash-<image_id>.img.gz | dd of=/dev/rsdXc bs=1m
   ```
   Example using sd0:
   ```shell
   gunzip -c /imaging/images/example/premade/install/resflash-amd64-1906MB-com0_115200-20200913_0853.img.gz | dd of=/dev/rsd0c bs=1m
   ```

### Upgrade resflash images

See [resflash instructions](https://gitlab.com/bconway/resflash#upgrades) for more information about upgrades using .fs images.

1. Run the **[upgrade_host.sh](#upgrade-host-script)** to build, configure, and apply the image to a target host
	```shell
	/imaging/scripts/upgrade_host.sh config_name host_name_or_ip ssh_key_path [-b|--build_vga] [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]
	```
	Example:
	```shell
	/imaging/scripts/upgrade_host.sh example example.domain.local /path/to/ssh_key.ssh -port 22227 -reboot
	```

### Recover a broken upgrade

The image upgrade process alternates between two partitions `d` and `e`, writing to the unused partition so that the current partition is left alone.
The partitions are referred to in the boot prompt (and the `/mbr/etc/boot.conf` file) as `hd0d` and `hd0e`.
If the upgrade process results in a failure to boot or a broken configuration, you can revert to the "previous" partition which was not updated.
This only works if at least one working image has already been installed (i.e., NOT on the initial install).
Access to the boot menu (`boot>`) is required for this process, that is, console access of some sort (serial console or other console).

Assuming that the `e` partition is your upgraded partition that is failing or broken set the boot to use the `d` partition (if not, change `hd0d` to `hd0e` below)
1. At the OpenBSD `boot>` prompt, type:
   ```shell
   set device hd0d
   ```
   and press enter to boot.\
   Note: There are only a few seconds at the `boot>` prompt before auto-boot starts.
   It's a good idea to press the space-bar as soon as the `boot>` prompt appears on the console to
   stop the auto-boot and give yourself some time to collect your thoughts before proceeding.

1. Before doing any diagnosis on your failed image, you will want to mount `/mbr` and edit `/mbr/etc/boot.conf` to point to the working boot device.
   After the the working image has finished booting, log in and edit `boot.conf`:
   ```shell
   mount /dev/sd0a /mbr
   vi /mbr/etc/boot.conf
   umount /mbr
   ```

### Mount resflash images

If images need to be manually validated before being installed, they can be mounted using resflash tools
**mount_resflash.sh** and **umount_resflash.sh**.
These tools are used internally by [config_image.sh](#image-configuration-script) to mount and configure the image.
The **mount_resflash.sh** tool returns the path where the image is mounted. Under this path the root of the image file system
is found under the `fs` directory.
The **umount_resflash.sh** tool will unmount all mounted resflash images.
See [resflash instructions](https://gitlab.com/bconway/resflash#other-build-tools) for information about mounting images.

Examples:
```shell
/imaging/resflash/mount_resflash.sh /imaging/images/example/resflash-amd64-1906MB-com0_115200-20240828_0516.fs
```

```shell
/imaging/resflash/umount_resflash.sh
```
