#!/bin/sh
#
# Configure OpenBSD machine to build images with resflash and automation built around build_premade.sh
# Source control path: https://gitlab.com/gregwinterstein/autoimage/-/raw/master/config_machine.sh
#
# Resflash:
#   https://gitlab.com/bconway/resflash
#
# Steps:
#   - Sets up file systems for building images
#   - Patches machine operating system (runs syspatch twice)
#   - Installs additional autoimage scripts from archive or source control
#   - Installs config from archive or source control
#   - Downloads and installs resflash
#   - Patches Resflash upgrade.sh to make new partition more prominent
#   - Configures resflash.conf if requested
#   - Downloads and modifies resflash-tools/build_premade.sh to build_premade_auto.sh
#   - Installs sources into /usr/src
#       Existing sources, if any, are removed
#       Sources are extracted from /tmp/{src,sys}.tar.gz if found, otherwise downloaded from /etc/installurl
#   - Attempts to patch sources
#   - Starts building release sets if requested
#
# Build machine with:
#   8 processors (1 core/processor)
#   10 GB (10240 MB) RAM
#   NAT networking
#   30 GB disk space
#   Remove sound card
#   Add USB 3 (or whatever you may need to write images to other media)
#
# Filesystem     Size  Mounted on
# /dev/wd0a      1.0G  /
# /dev/wd0d      1.0G  /var
# /dev/wd0e      3.0G  /usr
# /dev/wd0f      1.0G  /usr/X11R6
# /dev/wd0g      500M  /usr/local
# /dev/wd0h      3.0G  /usr/src
# /dev/wd0i      6.0G  /usr/obj
# /dev/wd0j      100M  /home
# /dev/wd0k      4.0G  /tmp
# /dev/wd0l      3.0G  /imaging                                               # ROOT_DIR
# /dev/wd0m      5.4G  /imaging/build (use all remaining for this partition)  # BUILD_DIR
#
# Install all sets except games (unless games are required)
#   -g*
#
# After installing OpenBSD:
# * Copy the following to /tmp
#   config_machine.sh                       (this script)
#
# * Optionally copy the following to /tmp
#   src.tar.gz                              (will be downloaded from /etc/installurl if not found)
#   sys.tar.gz                              (will be downloaded from /etc/installurl if not found)
#   config_*.tar.gz                         (configuration files for images, can be downloaded from source control if URL specified in -c command-line argument)
#
# * Set executable bit on this script
#   chmod a+x /tmp/config_machine.sh
#
# * Use config_machine.sh (this script) to do initial configuration
#   /tmp/config_machine.sh [-b|--build_release] [-r|--config_resflash] [-c config_archive_path] [-g|--gitlab_token]
#       -b|--build_release                   Optional switch that automatically triggers a release build as part of the script
#       -r|--config_resflash                 Optional switch that configures resflash with save_etc & save_etc (Change DEFAULT_RESFLASH_SAVE_ETC or DEFAULT_RESFLASH_SAVE_VAR in script to customize)
#       -c config_archive_path               Optional path to the config files (URL or local path) assumes local path of /tmp/config_*.tar.gz if not specified
#       -g|--gitlab_token                    Optional switch to force use of GitLab token when retrieving config files (token will be requested on console)
#                                               Currently only supports GitLab API tokens
#
#   Example:
#       /tmp/config_machine.sh --build_release --config_resflash --gitlab_token -c https://gitlab.com/api/v4/projects/<project_id>/repository/archive.tar.gz


# Resflash config defaults, change to customize -r (or leave -r out and handle this manually)
DEFAULT_RESFLASH_SAVE_ETC="save_etc='isakmpd/local.pub isakmpd/private/local.key'"
DEFAULT_RESFLASH_SAVE_VAR="save_var='db/host.random db/dhcpd.leases'"

ROOT_DIR="/imaging"
BUILD_DIR="${ROOT_DIR}/build"               # build directory used by build_release.sh
BUILD_DEST_DIR="${BUILD_DIR}/dst"           # build_release_auto.sh places install here
RESFLASH_RELDIR="${BUILD_DIR}/rel"          # build_release_auto.sh places release set here
RESFLASH_BASEDIR="${ROOT_DIR}/base"         # build_premade_auto.sh creates base install here
RESFLASH_DIR="${ROOT_DIR}/resflash"         # Resflash scripts location
RESFLASH_HOST_DIR="${RESFLASH_DIR}/host"    # Resflash host directory location (used to patch host files)
RESFLASH_HOST_UPGRADE="${RESFLASH_HOST_DIR}/upgrade.sh"     # Resflash host upgrade.sh script
RESFLASH_CONFIG_PATH="${RESFLASH_DIR}/etc/resflash.conf"    # Path to resflash config file (used to customize config if requested)
IMAGE_DIR="${ROOT_DIR}/images"              # Resflash will place built images here
CONFIG_DIR_NAME="config"
SCRIPT_DIR_NAME="scripts"
SCRIPT_DIR="${ROOT_DIR}/${SCRIPT_DIR_NAME}" # AutoImage scripts
CONFIG_FILE_PATTERN="/tmp/config_*.tar.gz"
AUTOIMAGE_SCRIPT_FILE_PATTERN="/tmp/scripts_*.tar.gz"
URL_STARTS_WITH_PATTERN="http*"

RESFLASH_ARCHIVE_URL="https://gitlab.com/bconway/resflash/-/archive/master/resflash-master.tar.gz"
RESFLASH_BUILD_PREMADE_URL="https://gitlab.com/bconway/resflash-tools/raw/master/build_premade.sh"
AUTOIMAGE_SCRIPT_ARCHIVE_URL="https://gitlab.com/api/v4/projects/20986724/repository/archive.tar.gz"
# Use refactor branch temporarily
# AUTOIMAGE_SCRIPT_ARCHIVE_URL="https://gitlab.com/api/v4/projects/20986724/repository/archive.tar.gz?sha=d5901c3689d1b282f4bf82b16cf46cb35033254f"

BUILD_PREMADE_ORIGINAL_NAME="build_premade.sh"
BUILD_PREMADE_AUTO_NAME="build_premade_auto.sh"
BUILD_PREMADE_PATCH="build_premade.sh.patch"
UPGRADE_PATCH="upgrade.sh.patch"
SOURCE_PATCH_SCRIPT="patch_source.sh"
SOURCE_PATCH_SCRIPT_PATH="${SCRIPT_DIR}/${SOURCE_PATCH_SCRIPT}"
BUILD_RELEASE_SCRIPT="build_release.sh"
BUILD_RELEASE_SCRIPT_PATH="${SCRIPT_DIR}/${BUILD_RELEASE_SCRIPT}"


#####################################################################
# Password methods borrowed from OpenBSD miniroot
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
ask_pass() {
        stty -echo
        IFS= read -r resp?"$1 "
        stty echo
        echo
}


#####################################################################
# Extract at folder from a .tar.gz archive to a specific location
# Remove archive file to clean up
# $1 = archive file name
# $2 = folder to extract from archive
# $3 = destination of folder extracted from archive
# Extracted to /tmp before being moved to destination
# Returns nothing
extract_archive() {
    local _archive_name=$1
    local _folder_name=$2
    local _destination_folder=$3
    local _EXTRACT_PATH="/tmp"
    local ARCHIVE_PATH=
    local ARCHIVE_PATH_PREFIX=

    # Find folder in archive paths (won't work if there are multiple sub-folders
    # with _folder_name, assume FIRST is the one we are looking for
    if [[ -n "${_folder_name}" ]]; then
        ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | grep "${_folder_name}"$ | head -n 1)
        ARCHIVE_PATH_PREFIX="${_EXTRACT_PATH}/${ARCHIVE_PATH%%/"${_folder_name}"}"
    else
        ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | head -n 1)
    fi

    # Extract archive into temporary location
    echo "Extracting ${_folder_name} archive: ${_archive_name}"
    cd "${_EXTRACT_PATH}"
    tar -zxf "${_archive_name}"

    # Move extracted files to correct location
    echo "Moving ${ARCHIVE_PATH} to ${_destination_folder}"
    mv "${ARCHIVE_PATH}" "${_destination_folder}"

    # Clean up archive
    rm -rf "${_archive_name}"
    if [[ -n "${ARCHIVE_PATH_PREFIX}" ]]; then
        rm -rf "${ARCHIVE_PATH_PREFIX}"
    fi
}


#####################################################################
# Search for an archive first locally, and then remotely (URL)
# Removes downloaded archive file after extraction
# $1 = local file pattern
# $2 = Remote URL used to retrieve file if no local file is found
# $3 = folder to extract from archive
# $4 = destination of folder extracted from archive
# $5 = GitLab token to use, if any
# Returns nothing
get_archive() {
    local _local_pattern=$1
    local _remote_url=$2
    local _folder_name=$3
    local _destination_folder=$4
    local _gitlab_token=$5

    local _EXTRACT_PATH="/tmp"
    local _ARCHIVE_NAME="archive.tar.gz"
    local _ARCHIVE_PATH="${_EXTRACT_PATH}/${_ARCHIVE_NAME}"

    local NO_LOCAL_ARCHIVE_FOUND=true
    if [[ -n "${_local_pattern}" ]]; then
        for ARCHIVE_FILE in ${_local_pattern}
        do
            # If file pattern doesn't exist only one file is returned and name = pattern
            # If pattern is a file name and not a pattern, the file will exist
            if [[ -f "${ARCHIVE_FILE}" ]]; then
                extract_archive "${ARCHIVE_FILE}" "${_folder_name}" "${_destination_folder}"
                NO_LOCAL_ARCHIVE_FOUND=false
            fi
        done
    fi

    if ${NO_LOCAL_ARCHIVE_FOUND}; then
        if [[ -n "${_remote_url}" ]]; then
            # Download scripts archive and extract
            echo "Downloading ${_folder_name} archive: ${_remote_url}"
            cd "${_EXTRACT_PATH}"
            if [[ -z "${_gitlab_token}" ]]; then
                wget -q "${_remote_url}" -O "${_ARCHIVE_PATH}"
            else
                wget -q --header="PRIVATE-TOKEN: ${_gitlab_token}" "${_remote_url}" -O "${_ARCHIVE_PATH}"
            fi

            extract_archive "${_ARCHIVE_PATH}" "${_folder_name}" "${_destination_folder}"
        fi
    fi
}

# Print a header using SEP
# $1 = header message
print_header() {
    printf "\n%s\n%b\n" ${SEP} "$1"
}

# Print a header using SMSEP
# $1 = header message
print_sm_header() {
    printf "\n%s\n%b\n" ${SMSEP} "$1"
}


SEP="=============================================================================="
SMSEP="-----------------------------------------------------------------"

EXIT_STATUS=0
ERROR_MESSAGES=
let ERROR_COUNT=0
BUILD_RELEASE=false
USE_GITLAB_TOKEN=false
CONFIG_PATH="${CONFIG_FILE_PATTERN}"
GITLAB_TOKEN=
CONFIGURE_RESFLASH=false
while [[ $# -gt 0 ]]
do
    case "$1" in
        -b) BUILD_RELEASE=true;;
        --build_release) BUILD_RELEASE=true;;
        -c) CONFIG_PATH="$2"; shift;;
        -g) USE_GITLAB_TOKEN=true;;
        --gitlab_token) USE_GITLAB_TOKEN=true;;
        -r) CONFIGURE_RESFLASH=true;;
        --config_resflash) CONFIGURE_RESFLASH=true;;
        -*) echo >&2 \
            "Usage: $0 [-b|--build_release] [-r|--config_resflash] [-c config_archive_path] [-g|--gitlab_token]"
            exit 1;;
        *)  break;;     # terminate while loop
    esac
    shift
done


print_header "Starting ${0}"
# Get token up-front
if ${USE_GITLAB_TOKEN}; then
    resp=
    ask_pass "Enter GitLab token for config files archive: '${CONFIG_PATH}':"
    GITLAB_TOKEN="${resp}"
fi


print_header "Configuring machine for building images"

# Set man pager to not clear screen on exit
echo "export MANPAGER=\"less -X\"" >> /root/.profile

# Configure release build directories
print_sm_header "Configure build directories"
echo "Adding 'noperm' to ${BUILD_DIR} mount in fstab"
sed -i -e "s@${BUILD_DIR} ffs rw,nodev,nosuid @${BUILD_DIR} ffs rw,nodev,nosuid,noperm @" /etc/fstab 2>/dev/null


echo "Remounting ${BUILD_DIR} to update 'noperm'"
umount ${BUILD_DIR}
mount ${BUILD_DIR}


echo "Setting permissions on ${BUILD_DIR}"
chown build ${BUILD_DIR}
chmod 700 ${BUILD_DIR}


echo "Creating release sub-directories"
mkdir ${BUILD_DEST_DIR}
mkdir ${RESFLASH_RELDIR}
mkdir ${IMAGE_DIR}


print_sm_header "Running syspatch"
syspatch
syspatch


# Install wget if needed, used to retrieve scripts, config, and patches
if ! /usr/sbin/pkg_info -e net/wget; then
    print_sm_header "Installing wget"
    /usr/sbin/pkg_add -v wget
fi

# Install expect, used in automated image upgrade scripts
if ! /usr/sbin/pkg_info -e lang/expect; then
    print_sm_header "Installing expect"
    /usr/sbin/pkg_add -v expect
fi

# Install scripts and config
print_sm_header "Search for config archive"
# shellcheck disable=SC2254
case "${CONFIG_PATH}" in
    ${URL_STARTS_WITH_PATTERN})
        get_archive "" "${CONFIG_PATH}" "${CONFIG_DIR_NAME}" "${ROOT_DIR}" "${GITLAB_TOKEN}";;

    *)
        get_archive "${CONFIG_PATH}" "" "${CONFIG_DIR_NAME}" "${ROOT_DIR}" "${GITLAB_TOKEN}";;
esac

print_sm_header "Search for script archive"
get_archive "${AUTOIMAGE_SCRIPT_FILE_PATTERN}" "${AUTOIMAGE_SCRIPT_ARCHIVE_URL}" "${SCRIPT_DIR_NAME}" "${ROOT_DIR}"

print_sm_header "Downloading resflash"
get_archive "" "${RESFLASH_ARCHIVE_URL}" "" "${RESFLASH_DIR}"

if [[ -f "${RESFLASH_HOST_UPGRADE}" ]]; then
    if [[ -f "${SCRIPT_DIR}/${UPGRADE_PATCH}" ]]; then
        echo "Patching ${RESFLASH_HOST_UPGRADE}"
        # Convert DOS to unix so patch applies cleanly
        tr -d '\15' < "${RESFLASH_HOST_UPGRADE}" > "${RESFLASH_HOST_UPGRADE}.tmp"
        rm "${RESFLASH_HOST_UPGRADE}"
        mv "${RESFLASH_HOST_UPGRADE}.tmp" "${RESFLASH_HOST_UPGRADE}"

        if patch "${RESFLASH_HOST_UPGRADE}" -i "${SCRIPT_DIR}/${UPGRADE_PATCH}" -o "${RESFLASH_HOST_UPGRADE}"; then
            echo "Patched ${RESFLASH_HOST_UPGRADE}"
            rm "${RESFLASH_HOST_UPGRADE}.orig"
        else
            ERROR_MESSAGES[ERROR_COUNT]="Patch '${UPGRADE_PATCH}' failed."
            let ERROR_COUNT+=1
            ERROR_MESSAGES[ERROR_COUNT]="${RESFLASH_HOST_UPGRADE} may be incorrect."
            let ERROR_COUNT+=1
        fi

        if [[ -f "${RESFLASH_HOST_UPGRADE}" ]]; then
            if [[ "$(stat -n -f '%Op' ${ROOT_DIR}/resflash/host/upgrade.sh)" -ne "100755" ]]; then
                echo "Fixing permissions on ${RESFLASH_HOST_UPGRADE}"
                chmod 755 "${RESFLASH_HOST_UPGRADE}"
            fi
        fi
    else
        echo ""
        echo "${UPGRADE_PATCH} file not found. Not patching ${RESFLASH_HOST_UPGRADE}"
        ERROR_MESSAGES[ERROR_COUNT]="${UPGRADE_PATCH} file not found. Not patching ${RESFLASH_HOST_UPGRADE}"
        let ERROR_COUNT+=1
        ERROR_MESSAGES[ERROR_COUNT]="${RESFLASH_HOST_UPGRADE} is unpatched."
        let ERROR_COUNT+=1
    fi
fi

if ${CONFIGURE_RESFLASH}; then
    if [[ -f "${RESFLASH_CONFIG_PATH}" ]]; then
        echo "Configuring ${RESFLASH_CONFIG_PATH}"
        # Remove non-commented save_etc & save_var lines from resflash.conf
        sed -i -e "/^\s*save_etc=/d" -e "/^\s*save_var=/d" "${RESFLASH_CONFIG_PATH}" 2>/dev/null

        # Append to end of file (would be ideal to write these after appropriate comments, but much easier to append)
        printf "\n\n# Custom save targets\n%s\n%s\n" "${DEFAULT_RESFLASH_SAVE_ETC}" "${DEFAULT_RESFLASH_SAVE_VAR}" >> "${RESFLASH_CONFIG_PATH}"
    else
        echo "${RESFLASH_CONFIG_PATH} path not found. Not configuring resflash.conf"
    fi
fi

print_sm_header "Downloading resflash ${BUILD_PREMADE_ORIGINAL_NAME} script"
if [[ ! -d "${SCRIPT_DIR}" ]]; then
    echo "Creating ${SCRIPT_DIR}"
    mkdir -p "${SCRIPT_DIR}"
fi

# Clean up any old scripts
rm ${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}.{old,orig}

if [[ -f "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}" ]]; then
    echo "Renaming existing script ${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}"
    mv "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}" "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}.old"
fi

cd ${SCRIPT_DIR}
wget -q "${RESFLASH_BUILD_PREMADE_URL}"
# Convert DOS to unix so patch applies cleanly
tr -d '\15' < "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}" > "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}.tmp"
rm "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}"
mv "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}.tmp" "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}"

if [[ -f "${SCRIPT_DIR}/${BUILD_PREMADE_PATCH}" ]]; then
    print_sm_header "Patching ${BUILD_PREMADE_ORIGINAL_NAME} for our own nefarious purposes"

    if patch "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}" -i "${SCRIPT_DIR}/${BUILD_PREMADE_PATCH}" -o "${SCRIPT_DIR}/${BUILD_PREMADE_AUTO_NAME}"; then
        mv "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}" "${SCRIPT_DIR}/${BUILD_PREMADE_ORIGINAL_NAME}.orig"
    else
        ERROR_MESSAGES[ERROR_COUNT]="Patch '${BUILD_PREMADE_PATCH}' failed."
        let ERROR_COUNT+=1
        ERROR_MESSAGES[ERROR_COUNT]="${BUILD_PREMADE_AUTO_NAME} is not available."
        let ERROR_COUNT+=1
    fi
else
    echo ""
    echo "${BUILD_PREMADE_PATCH} file not found. Not patching build_premade.sh"
    ERROR_MESSAGES[ERROR_COUNT]="${BUILD_PREMADE_PATCH} file not found. Not patching build_premade.sh"
    let ERROR_COUNT+=1
    ERROR_MESSAGES[ERROR_COUNT]="${BUILD_PREMADE_AUTO_NAME} is not available."
    let ERROR_COUNT+=1
fi


# Set scripts to be executable
for SCRIPT_FILE in "${SCRIPT_DIR}"/*.sh;
do
    chmod a+x "${SCRIPT_FILE}"
done


# Install and patch source files
if [[ -f "${SOURCE_PATCH_SCRIPT_PATH}" ]]; then
    ${SOURCE_PATCH_SCRIPT_PATH}
else
    echo
    echo "${SOURCE_PATCH_SCRIPT_PATH} file not found. Not installing and patching sources."
    ERROR_MESSAGES[ERROR_COUNT]="${SOURCE_PATCH_SCRIPT_PATH} file not found. Sources not installed or patched."
    let ERROR_COUNT+=1
fi

print_header "Configuration completed.\n\n"

if ${BUILD_RELEASE}; then
    if [[ -f "${SOURCE_PATCH_SCRIPT_PATH}" ]]; then
        if [[ -f "${BUILD_RELEASE_SCRIPT_PATH}" ]]; then
            print_sm_header "Starting release build"
            ${BUILD_RELEASE_SCRIPT_PATH}
            print_sm_header "Release build completed\nNext steps:"
        fi
    else
        echo ""
        echo "${SOURCE_PATCH_SCRIPT_PATH} file not found. Not building release without patched sources."
        ERROR_MESSAGES[ERROR_COUNT]="${SOURCE_PATCH_SCRIPT_PATH} file not found. Not building release without patched sources."
        let ERROR_COUNT+=1
    fi
else
    # Display additional information regarding building releases
    cat << EOF
Next steps:
* If you are NOT using the standard paths in the file system layout,
    modify paths in scripts to match your paths before building release:
    ${BUILD_RELEASE_SCRIPT_PATH}
        BUILD_DEST_DIR="${BUILD_DEST_DIR}"
        RESFLASH_RELDIR="${RESFLASH_RELDIR}"

    ${ROOT_DIR}/scripts/${BUILD_PREMADE_AUTO_NAME}
        RELDIR="${RESFLASH_RELDIR}"
        DESTDIR="${RESFLASH_BASEDIR}"
        CONFIG_SCRIPT=${ROOT_DIR}/scripts/config_image.sh

    ${ROOT_DIR}/scripts/config_image.sh
        CONFIG_ROOT="${ROOT_DIR}/config/"
        RESFLASH_PATH="${RESFLASH_DIR}"

* Build a release (takes a while, ~ 2-8 hours, 12+ hours for i386)
    ${BUILD_RELEASE_SCRIPT_PATH}

EOF
fi

# Display additional information
if ! ${CONFIGURE_RESFLASH}; then
    cat << EOF
* Configure resflash with files to save.
   Note: Saved files will survive both reboots and upgrades. Do not save anything that will need to change in upgrades.
   Example:
    vi ${ROOT_DIR}/resflash/etc/resflash.conf
        save_etc='isakmpd/local.pub isakmpd/private/local.key'
        save_var='db/host.random db/dhcpd.leases'

EOF
fi

cat << EOF
* Build & configure images using ${BUILD_PREMADE_AUTO_NAME} script
    Configures both .img & .fs images (takes about 5 minutes)
    Note: Serial-only routers do not need VGA image, -build_vga is only for machines with VGA output
    cd ${ROOT_DIR}/images/config_name                                                    # IMAGE_DIR
    ../../scripts/${BUILD_PREMADE_AUTO_NAME} resflash_dir config_name [-b|--build_vga]     # RESFLASH_DIR

    Example:
    cd ${ROOT_DIR}/images/myconfig
    ../../scripts/${BUILD_PREMADE_AUTO_NAME} ${ROOT_DIR}/resflash myconfig

    Example:
    cd ${ROOT_DIR}/images/myconfig
    ../../scripts/${BUILD_PREMADE_AUTO_NAME} ${ROOT_DIR}/resflash myconfig -build_vga

* Copy install image to some media (CF, USB, Disk, etc) at sd0 (change sd0 to your device) (CF takes about 5 minutes)
    Newer version of ${BUILD_PREMADE_AUTO_NAME} compresses images into images/myconfig/premade/install
    and images/myconfig/premade/upgrade
    Example:
    gunzip -c ${ROOT_DIR}/images/myconfig/premade/install/resflash-amd64-1906MB-com0_115200-20200913_0853.img.gz | dd of=/dev/rsd0c bs=1m

* See resflash instructions for upgrades using .fs images:
    https://gitlab.com/bconway/resflash#upgrades

* To rebuild the release for updates:
    ${SOURCE_PATCH_SCRIPT_PATH} && ${BUILD_RELEASE_SCRIPT_PATH}
    Note: You can speed up patching by copying SHA256.sig, src.tar.gz, and sys.tar.gz to /tmp to avoid downloads
EOF

if [[ ${ERROR_COUNT} -gt 0 ]]; then
    EXIT_STATUS=1
    print_sm_header "{0} Errors:"
    printf " %s\n" "${ERROR_MESSAGES[@]}"
fi
echo ""
echo "Ending ${0}"
echo ""
echo "${SEP}"
exit ${EXIT_STATUS}