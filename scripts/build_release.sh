#!/bin/sh
# Build release sets
#
# Usage:
#   ./build_release.sh [-n|--no_jobs]
#       -n|--no_jobs                        Optional switch to prevent specifying number of jobs passed to make (-j)
#                                               Default is to determine number of jobs based on CPUs online
#                                               and physical memory to attempt to speed up build process
#
# Used in config_machine.sh for building release sets after patching
# Can be used stand-alone to building a new release after patching
#   Example: /imaging/scripts/patch_source.sh && /imaging/scripts/build_release.sh
#

# Set paths
ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
BUILD_DEST_DIR="${ROOT_DIR}/build/dst"
RESFLASH_RELDIR="${ROOT_DIR}/build/rel"

# Set start time stamp
START_DATE=$(date)
START_SECONDS=$SECONDS
EXIT_STATUS=0
JOBS=

if [[ "${1}" != "-n" && "${1}" != "--no_jobs" ]] ; then
    # Calculate number of jobs to use with make
    # based on CPUs online and GB of RAM
    # Leave margin of 2 to avoid running out of memory
    # Jobs specified = Max(CPUsOnline, GBofRAM) - 2
    # -j is only specified if we can specify more than one job
    cpuCount=$(sysctl hw.ncpuonline | cut -d = -f 2)
    memInGB=$(printf "%0.f" $(echo "$(sysctl hw.physmem | cut -d = -f 2) / 1024 / 1024 / 1024" | bc -l))
    if [[ $cpuCount -gt $memInGB ]] ; then
        cpuCount=$memInGB
    fi
    cpuCount=$(echo "$cpuCount - 2" | bc)
    if [[ $cpuCount -gt 1 ]] ; then
        JOBS="-j ${cpuCount} "
        echo "Using: '${JOBS}'"
    fi
fi

# Cleanup obj files
cd /usr/obj && mkdir -p .old && mv * .old && rm -rf .old &
cd /usr/src && make ${JOBS}obj 
cd /usr/src/etc && env DESTDIR=/ make distrib-dirs
cd /usr/src && make ${JOBS}build
EXIT_STATUS=$?

# Check build status, skip release if build failed
if [[ $EXIT_STATUS -eq 0 ]]; then
    # Set initial build finish date
    BUILD_COMPLETE_DATE=$(date)

    export DESTDIR="${BUILD_DEST_DIR}"; export RELEASEDIR="${RESFLASH_RELDIR}"
    test -d ${DESTDIR} && mv ${DESTDIR} ${DESTDIR}- && rm -rf ${DESTDIR}- &
    # Avoid a race condition with directory creation, so sleep 1/2 minute
    sleep 30
    mkdir -p ${DESTDIR} ${RELEASEDIR}

    cd /usr/src/etc && make ${JOBS}release

    # Set release complete date
    REL_COMPLETE_DATE=$(date)

    cd /usr/src/distrib/sets && sh checkflist
    unset RELEASEDIR DESTDIR
else
    # Set completed dates
    BUILD_COMPLETE_DATE=$(date)
    REL_COMPLETE_DATE=$(date)
fi

END_SECONDS=$SECONDS
ELAPSED=$((END_SECONDS - START_SECONDS))

echo "\nStart:             ${START_DATE}"
echo "Build completed:   ${BUILD_COMPLETE_DATE}"
echo "Release completed: ${REL_COMPLETE_DATE}"
printf 'Elapsed time:      %d:%02d:%02d\n' $((ELAPSED / 3600)) $((ELAPSED / 60 % 60)) $((ELAPSED % 60))
if [[ $EXIT_STATUS -ne 0 ]]; then
    echo "ERROR: Build Failed."
fi
exit ${EXIT_STATUS}
