#!/bin/sh
#
# Configures a (non-imaged) host based on config files
# Self-contained script to automate configuration to reduce effort to create hosts
# Host-based analog of config_image.sh
#
# Configuration based on files in: source_control/config/<config_name>
#
# Configuration steps:
#   - Gather user passwords, private keys, IPSec PSKs, and additional passwords (ADMIN_USER, ADMIN_ID, IPSEC_PSK_NAME, PASSWORD_PROMPT, PASSWORD_FILE)
#   - Add admin users (ADMIN_USER, ADMIN_ID)
#   - Add no-password users (NO_PW_USER, NO_PW_USER_ID, NO_PW_USER_HOME, NO_PW_USER_SHELL, NO_PW_USER_DESC)
#   - Change root user name if requested (CHANGE_ROOT_NAME_TO_HOSTNAME)
#   - Set root user to not clear screen after exiting `man`
#   - Set vi tab stops to 4 for root
#   - Set root password
#   - Update password database
#   - Set no-password user groups (NO_PW_USER_GROUPS)
#   - Set admin users to not clear screen after exiting `man`
#   - Set vi tab stops to 4 for admin users
#   - Add admin user public keys (authorized_keys)
#   - Copy files/doas.conf or copy from /etc/examples
#   - Copy/create ssh_banner, sshd_config (SSH_BANNER, SSHD_CONFIG)
#   - Create /etc/mygate (MY_GATE)
#   - Create /etc/myname (MY_NAME)
#   - Copy files/hostname.*
#     Rename hostname.* files with correct interface name if INTERFACE is defined
#     Replace interface place-holders if INTERFACE is defined
#   - Copy files/dhcpd.conf
#   - Copy files/hosts or create default hosts file with localhost if it doesn't already exist
#   - Copy/create /etc/isakmpd/isakmpd.policy file (ISAKMPD_POLICY)
#   - Copy IPSec config files (IPSEC_FILE_PATTERN)
#     Replace PSK place-holders if IPSEC_PSK_NAME defined
#   - Copy pf.conf
#     Replace interface place-holders if INTERFACE is defined
#   - Copy/create /etc/ntpd.conf (NTP_CONF)
#   - Install additional packages (ADDL_PACKAGES)
#   - Copy/create /etc/rc.conf.local (RC_CONF_LOCAL)
#   - Copy/create /etc/rc.local (RC_LOCAL)
#   - Copy/create /etc/sysctl.conf (SYSCTL_ENABLE_IPV4_FORWARDING, SYSCTL_ENABLE_IPV6_FORWARDING)
#   - Copy/create unbound configuration (CONFIGURE_UNBOUND, UNBOUND_CONF)
#     Replace interface place-holders if INTERFACE is defined in unbound.conf
#     Retrieves current root hints file
#     Runs unbound-anchor to set the root trust anchor for DNSSEC validation
#     You are responsible for enabling unbound in /etc/rc.conf.local (RC_CONF_LOCAL),
#     configuring resolv.conf, and any necessary changes to dhcpd.conf (MISC_ETC_FILE)
#   - Copy additional miscellaneous files to /etc (MISC_ETC_FILE, MISC_ETC_PERMS, MISC_ETC_OWNER)
#   - Copy additional miscellaneous files to /etc (MISC_MISC_FILE, MISC_MISC_PERMS, MISC_MISC_OWNER)
#   - Copy additional script files to SCRIPT_PATH (SCRIPT_FILE, SCRIPT_PERMS, SCRIPT_OWNER, SCRIPT_PATH)
#   - Add additional root crontab entries (ADDL_CRON_PATH)
#   - Replace passwords in files if specified (PASSWORD_FILE, PASSWORD_PROMPT)
#   - Set timezone if specified (TIMEZONE_PATH)
#   - Run post-install scripts (POST_INSTALL_ACTION, POST_INSTALL_SCRIPT, POST_INSTALL_DESC)
#
#
# Host config file:
#   source_control/config/<config_name>/<config_name>.conf
#       Sets configuration items for the specific configuration instance
#
# Directory structure:
#   source_control/config/<config_name>/files
#       ... any config files to be copied (specified in MISC_ETC_*, MISC_MISC_*, & SCRIPT_* below) ...
#
# Usage:
#   ./config_host.sh config_name [-c config_archive_path] [-g|--gitlab_token]
#       config_name                         Name of configuration name used to find configuration file(s)
#       -c config_archive_path              Optional path to the config files (URL or local path) assumes local path of /tmp/*config-<config_name>*.tar.gz if not specified
#                                           URL format can specify the entire archive, but can also specify the <config_name> folder, e.g.,
#                                           https://gitlab.com/<account_name>/<project_name>/-/archive/master/<project_name>-master.tar.gz?path=config/<config_name>
#       -g|--gitlab_token                   Optional switch to force use of GitLab token when retrieving config files (token will be requested on console)
#                                               Currently only supports GitLab API tokens
#
# Example: ./config_host.sh k2.5 -c /tmp/config-k2.5.tar.gz -g
# Example: ./config_host.sh k2.5 -g -c https://gitlab.com/<account_name>/<project_name>/-/archive/master/<project_name>-master.tar.gz?path=config/k2.5
#

URL_STARTS_WITH_PATTERN="http*"
CONFIG_FILE_PATTERN="/tmp/*config-*.tar.gz"
ROOT_HINTS_URL="http://www.internic.net/domain/named.root"

SEP="=============================================================================="
SMSEP="-----------------------------------------------------------------"
TSEP="+++++"


#####################################################################
# Password methods borrowed from OpenBSD miniroot
#####################################################################
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
#
ask_pass() {
        stty -echo
        IFS= read -r resp?"$1 "
        stty echo
        echo
}


# Ask for a password twice, saving the input in $_password.
ask_password() {
        local _q=$1

        while :; do
                ask_pass "$_q (will not echo)"
                # $resp is assigned by ask_pass, disable shellcheck warning
                # shellcheck disable=SC2154
                _password=$resp

                ask_pass "$_q (again)"
                [[ $resp == "$_password" ]] && break

                echo "Passwords do not match, try again."
        done
}


# Ask for input twice, saving input in $resp, without disabling echo
ask_input() {
    local _q=$1
    local _temp_resp

    while :; do
        read -r resp?"$_q "
        _temp_resp=$resp

        read -r resp?"$_q (again) "
        [[ $resp == "$_temp_resp" ]] && break

        echo "Values do not match, try again."
    done
}


encr_pwd() {
        local _p=$1

        if [[ -z $_p ]]; then
                echo '*'
        elif [[ $_p == \$2?\$[0-9][0-9]\$* && ${#_p} -gt 40 ||
                $_p == '*************' ]]; then
                echo "$_p"
        else
                encrypt -b a -- "$_p"
        fi
}


usage() {
    printf >&2 "Usage: %s config_name [-c config_archive_path] [-g|--gitlab_token]\n       config_name                         Name of configuration name used to find configuration file(s)\n       -c config_archive_path              Optional path to the config files (URL or local path) assumes local path of /tmp/*config-<config_name>*.tar.gz if not specified\n                                           URL format can specify the entire archive, but can also specify the <config_name> folder, e.g.,\n                                           https://gitlab.com/<account_name>/<project_name>/-/archive/master/<project_name>-master.tar.gz?path=config/<config_name>\n       -g|--gitlab_token                   Optional switch to force use of GitLab token when retrieving config files (token will be requested on console)\n                                               Currently only supports GitLab API tokens\n\nExample: %s k2.5 -c /tmp/config-k2.5.tar.gz -g\n" "${0}" "${0}"
    exit 1
}


# Print a header using TSEP
# $1 = header message
print_header() {
    printf "\n%s %s %s\n" ${TSEP} "$1" ${TSEP}
}


capitalize() {
  typeset -u first
  first=${1%"${1#?}"}
  print "${first}${1#?}"
}


# Replace interface name place-holders with interface name defined in INTERFACE array
# Pass file name to replace as first argument
replace_interface() {
    local _FILE_NAME=$1
    local _local_index
    local _file_type

    if [[ ${#INTERFACE[*]} -gt 0 ]]; then
        _file_type=$(file -b "${_FILE_NAME}")
        if [ "${_file_type##*text*}" ]; then
            echo "Skipping interface replacement in non-text file: '${_FILE_NAME}'"
        else
            echo "Replacing interface place-holders in ${_FILE_NAME##*/}"
            # Reverse array order to prevent single-digit indexes from prematurely
            # replacing double-digit indexes (i.e., '1' replacing '10')
            let _local_index=${#INTERFACE[*]}
            while [[ $_local_index -gt 0 ]]; do
                let _local_index-=1
                # Replace INTERFACE.$_local_index with ${INTERFACE[$_local_index]}
                local SRC="INTERFACE.${_local_index}"
                local DST="${INTERFACE[$_local_index]}"
                sed -i -e "s@$SRC@$DST@g" "${_FILE_NAME}" 2>/dev/null
            done
        fi
    fi
}


# Replace psk name place-holders with user-specified values
# Pass file name to replace as first argument
replace_psk_place_holders() {
    local _FILE_NAME=$1
    local _local_index
    if [[ ${#IPSEC_PSK_NAME[*]} -gt 0 && ${#IPSEC_PSK[*]} -gt 0 ]]; then
        echo "Replacing PSK place-holders in '${_FILE_NAME##*/}'"
        let _local_index=0
        while [[ $_local_index -lt ${#IPSEC_PSK_NAME[*]} ]]; do
            local SRC="${IPSEC_PSK_NAME[$_local_index]}"
            local DST="${IPSEC_PSK[$_local_index]}"
            sed -i -e "s@$SRC@$DST@g" "${_FILE_NAME}" 2>/dev/null
            let _local_index+=1
        done
    fi
}


# Replace all password placeholders specified in PASSWORD_FILE files
# Each entry represents a PASSWORD_NAME.X to be replaced in one or more files
# PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
# PASSWORD_PASS[x]      = Contains password value from user prompt
replace_all_password_place_holders() {
    local _cur_file
    local _cur_files
    local _all_files
    local _unique_files

    # Flatten list of files
    if [[ ${#PASSWORD_FILE[*]} -gt 0 && ${#PASSWORD_PASS[*]} -gt 0 ]]; then
        local _file
        local _index
        let _index=0
        for _file in "${PASSWORD_FILE[@]}"
        do
            OLD_IFS=$IFS
            IFS="|"
            # This is an array and can't be quoted
            # shellcheck disable=SC2086
            set -A _cur_files ${_file}
            IFS=$OLD_IFS
            for _cur_file in "${_cur_files[@]}"
            do
                _all_files[_index]=$_cur_file
                let _index+=1
            done
        done

        # Get list of unique files
        NL=$(printf '\n.')
        NL=${NL%?}
        OLD_IFS=$IFS
        IFS=$NL
        # This is an array and can't be quoted
        # shellcheck disable=SC2046
        set -A _unique_files $( for _cur_file in "${_all_files[@]}"; do printf "%s\n" "${_cur_file}"; done | sort -u )
        IFS=$OLD_IFS

        # Replace placeholders in list of unique files
        for _file in "${_unique_files[@]}"
        do
            replace_password_place_holders "${_file}"
        done
    fi
}


# Replace password tokens in file
# Each entry represents a PASSWORD_NAME.X (e.g., PASSWORD_NAME.0, PASSWORD_NAME.1, etc) to be replaced in one or more files
# PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
# PASSWORD_PASS[x]      = Contains password value from user prompt
#
# $1 = File name (full path) be replaced
replace_password_place_holders() {
    local _FILE_NAME=$1
    local _temp_file_pass
    local _temp_file_out
    local _file_index
    local _file_mode
    local _file_owner
    local _file_type

    if [[ -f "${_FILE_NAME}" ]]; then
        _file_type=$(file -b "${_FILE_NAME}")
        if [ "${_file_type##*text*}" ]; then
            echo "Skipping password replacement in non-text file: '${_FILE_NAME}'"
        else
            echo "Replacing password place holders in '${_FILE_NAME}'"
            _temp_file_out=$(mktemp)
            # Copy should change ownership to current user, but not file permissions
            cp "${_FILE_NAME}" "${_temp_file_out}"
            _file_mode=$(stat -f "%#OLp" "${_FILE_NAME}")
            _file_owner=$(stat -f "%Su:%Sg" "${_FILE_NAME}")

            if [[ $(echo "${_file_mode}" | cut -b 2) -lt 6 ]]; then
                # Grant write permissions to current user
                chmod 0600 "${_temp_file_out}"
            fi

            # Reverse array order to prevent single-digit indexes from prematurely
            # replacing double-digit indexes (i.e., '1' replacing '10')
            let _file_index=${#PASSWORD_FILE[*]}
            while [[ $_file_index -gt 0 ]]; do
                let _file_index-=1
                local _cur_files
                local _cur_file
                local _temp_file_pass
                OLD_IFS=$IFS
                IFS="|"
                # This is an array and can't be quoted
                # shellcheck disable=SC2086
                set -A _cur_files ${PASSWORD_FILE[$_file_index]}
                IFS=$OLD_IFS

                for _cur_file in "${_cur_files[@]}"
                do
                    if [[ "${_FILE_NAME}" == "${_cur_file}" ]]; then
                        # Write password value to tempfile, escaping ampersands which cause issues in awk replacement
                        _temp_file_pass=$(mktemp)
                        printf "%s\n" "${PASSWORD_PASS[$_file_index]}" | perl -pe 's/&/\\&/g' >> "${_temp_file_pass}"

                        # sed can't be used to do this because we can't know
                        # if there are any delimiters in the password itself
                        lineCount=$(printf "%s" "$(wc -l "${_temp_file_out}")" | sed 's/^[ \t]*//;s/[ \t]*$//' | cut -d' ' -f 1)
                        hasTrailingNewLine=$(printf "%s" "$(tail -c1 "${_temp_file_out}" | wc -l)" | sed 's/^[ \t]*//;s/[ \t]*$//')

                        awk -F '\n' -v pattern="PASSWORD_NAME\.${_file_index}" -v lineCount="${lineCount}" -v hasTrailingNewLine="${hasTrailingNewLine}" '
                            BEGIN { OFS = FS }
                            NR == FNR { password = $0; next }
                            {
                                if (match($0, pattern) > 0)
                                {
                                    gsub(pattern, password);
                                }
                                printf("%s", $0);
                                isLastLine = (NR > lineCount);
                                if (!isLastLine || (isLastLine && hasTrailingNewLine > 0))
                                {
                                    printf("\n");
                                }
                            }' "${_temp_file_pass}" "${_temp_file_out}" > "${_temp_file_out}.new" && mv "${_temp_file_out}.new" "${_temp_file_out}"

                        # Delete temp password file
                        rm -f "${_temp_file_pass}"
                    fi
                done
            done

            mv "${_temp_file_out}" "${_FILE_NAME}"
            if [[ "$(stat -f "%#OLp" "${_FILE_NAME}")" != "${_file_mode}" ]] ; then
                chmod "${_file_mode}" "${_FILE_NAME}"
            fi

            if [[ "$(stat -f "%Su:%Sg" "${_FILE_NAME}")" != "${_file_owner}" ]] ; then
                chown "${_file_owner}" "${_FILE_NAME}"
            fi
        fi
    fi
}


# Copy configuration files, setting permissions and ownership
# $1 = destination path (no trailing slash)
# $2 = list of file names to copy from $FILES_PATH
# $3 = list of associated file permissions to apply (if any)
# $4 = list of associated file owners to apply (if any)
copy_files() {
    local _dest=$1
    local _fileNames
    local _filePerms
    local _fileOwners

    # Note: set -A will lose any null/blank array entries. This will break the "shared"
    # index between _fileNames, _filePerms, & _fileOwners.
    # This makes all _filePerms & _fileOwners entries required and non-empty/null
    # These argument variables are arrays, so they can't be quoted or they will not assign properly
    # shellcheck disable=SC2086
    set -A _fileNames $2
    # shellcheck disable=SC2086
    set -A _filePerms $3
    # shellcheck disable=SC2086
    set -A _fileOwners $4

    if [[ ${#_fileNames[*]} -gt 0 ]]; then
        local _index
        let _index=0
        while [[ $_index -lt ${#_fileNames[*]} ]]; do
            local _filePerm=""
            if [[ ${#_filePerms[*]} -gt 0 ]]; then
                _filePerm="${_filePerms[$_index]}"
            fi

            local _fileOwner=""
            if [[ ${#_fileOwners[*]} -gt 0 ]]; then
                _fileOwner="${_fileOwners[$_index]}"
            fi

            copy_file "${FILES_PATH}/${_fileNames[$_index]}" "${_dest}/${_fileNames[$_index]}" "${_filePerm}" "${_fileOwner}"
            let _index+=1
        done
    fi
}


# Copy a configuration file, setting permissions and ownership
# $1 = source file name (full path)
# $2 = destination file name (full path)
# $3 = file permission to apply (if any)
# $4 = file owner to set (if any)
copy_file() {
    local _fileName=$1
    local _destFileName=$2
    if [ "${_destFileName}" == "${_destFileName#/}" ]; then
        _destFileName="/${_destFileName}"
    fi
    local _destDirName
    _destDirName=$(dirname "${_destFileName}")
    local _filePerm=$3
    local _fileOwner=$4

    if [[ -f "${_fileName}" ]]; then
        if [[ -n "${_destFileName}" ]]; then
            if [[ ! -d "${_destDirName}" ]]; then
                echo "Creating path '${_destDirName}'"
                mkdir -p "${_destDirName}"
            fi

            echo "Copying '$(basename "${_fileName}")'"
            cp "${_fileName}" "${_destFileName}"
            replace_interface "${_destFileName}"

            if [[ -n "${_filePerm}" ]]; then
                echo "Setting permissions '${_filePerm}' on ${_destFileName}"
                chmod "${_filePerm}" "${_destFileName}"
            fi

            if [[ -n "${_fileOwner}" ]]; then
                echo "Setting owner '${_fileOwner}' on ${_destFileName}"
                chown "${_fileOwner}" "${_destFileName}"
            fi

            echo ""
        fi
    else
        printf '\033[1mWARNING: File not copied. Source file not found for '\''%s'\''\033[0m\n' "${_fileName}"
    fi
}


# Extract at folder from a .tar.gz archive to a specific location
# Remove archive file to clean up
# $1 = archive file name
# $2 = folder to extract from archive
# $3 = destination of folder extracted from archive
# Extracted to /tmp before being moved to destination
# Returns nothing
extract_archive() {
    local _archive_name=$1
    local _folder_name=$2
    local _destination_folder=$3
    local _EXTRACT_PATH="/tmp"
    local ARCHIVE_PATH=
    local ARCHIVE_PATH_PREFIX=

    if [[ -s "${_archive_name}" ]]; then
        # Find folder in archive paths (won't work if there are multiple sub-folders
        # with _folder_name, assume FIRST is the one we are looking for
        if [[ -n "${_folder_name}" ]]; then
            # shellcheck disable=SC2086
            ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | grep /${_folder_name}$ | head -n 1)
            ARCHIVE_PATH_PREFIX="${_EXTRACT_PATH}/${ARCHIVE_PATH%%/"${_folder_name}"}"
        else
            ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | head -n 1)
        fi
        # Extract archive into temporary location
        echo "Extracting ${_folder_name} archive: ${_archive_name}"
        cd "${_EXTRACT_PATH}"
        tar -zxf "${_archive_name}"

        # Move extracted files to correct location
        echo "Moving ${ARCHIVE_PATH} to ${_destination_folder}"
        mv "${ARCHIVE_PATH}" "${_destination_folder}"

        # Clean up archive
        echo "Clean up archive: '${_archive_name}'"
        rm -rf "${_archive_name}"
        if [[ -n "${ARCHIVE_PATH_PREFIX}" ]]; then
            echo "Clean up archive prefix path: '${ARCHIVE_PATH_PREFIX}'"
            rm -rf "${ARCHIVE_PATH_PREFIX}"
        fi
    else
        echo "Cannot find valid archive file: '${_archive_name}'"
        exit 1
    fi
}


# Search for an archive first locally, and then remotely (URL)
# Removes downloaded archive file after extraction
# $1 = archive path (local path or URL)
# $2 = folder to extract from archive
# $3 = destination of folder extracted from archive
# $4 = GitLab token to use, if any
# Returns nothing
get_archive() {
    local _archive_path=$1
    local _folder_name=$2
    local _destination_folder=$3
    local _gitlab_token=$4

    local _EXTRACT_PATH="/tmp"
    local _ARCHIVE_NAME="archive.tar.gz"
    local _ARCHIVE_PATH="${_EXTRACT_PATH}/${_ARCHIVE_NAME}"

    # shellcheck disable=SC2254
    case "${_archive_path}" in
        ${URL_STARTS_WITH_PATTERN})
            # Download archive and extract
            echo "Downloading ${_folder_name} archive: ${_archive_path}"
            cd "${_EXTRACT_PATH}"
            if [[ -z "${_gitlab_token}" ]]; then
                wget -q "${_archive_path}" -O "${_ARCHIVE_PATH}"
            else
                wget -q --header="PRIVATE-TOKEN: ${_gitlab_token}" "${_archive_path}" -O "${_ARCHIVE_PATH}"
            fi

            if [[ -s "${_ARCHIVE_PATH}" ]]; then
                extract_archive "${_ARCHIVE_PATH}" "${_folder_name}" "${_destination_folder}"
            else
                echo "Failed to download '${_archive_path}'"
                exit 1
            fi
            ;;

        *)
            # Check for pattern
            if [[ "${_archive_path}" == *+(\*|\?)* ]]; then
                for ARCHIVE_FILE in ${_archive_path}
                do
                    # If file pattern doesn't exist only one file is returned and name = pattern
                    # If pattern is a file name and not a pattern, the file will exist
                    if [[ -s "${ARCHIVE_FILE}" ]]; then
                        extract_archive "${ARCHIVE_FILE}" "${_folder_name}" "${_destination_folder}"
                    fi
                done
            else
                # Not a pattern
                if [[ -s "${_archive_path}" ]]; then
                    extract_archive "${_archive_path}" "${_folder_name}" "${_destination_folder}"
                else
                    echo "Archive not found: '${_archive_path}'"
                    exit 1
                fi
            fi
            ;;
    esac
}

# Download and install a package from the specified URL
# $1 = Package specification (pipe-delimited packageName|packageUrl|packageKeyUrl)
# Returns nothing
install_package_by_url() {
    local _package_spec=$1
    local _pkg_info=
    local _package_name=
    local _package_url=
    local _package_key_url=
    local _package_path=
    local _package_file=
    local package_file=
    local _key_path=
    local _key_file=
    local key_file=
    local _cur_path=

    # Parse pipe-delimited package spec into components
    OLD_IFS=$IFS
    IFS="|"
    # This doesn't work when quoted
    # shellcheck disable=SC2086
    set -A _pkg_info ${_package_spec}
    IFS=$OLD_IFS

    _package_name="${_pkg_info[0]}"
    _package_url="${_pkg_info[1]}"
    _package_key_url="${_pkg_info[2]}"

    # Download package in temp path
    _cur_path=$(pwd)
    _package_path=$(mktemp -d)
    cd "${_package_path}"
    wget -q "${_package_url}"
    cd "${_cur_path}"

    for package_file in "${_package_path}/"*.tgz; do
        _package_file="${package_file}"
    done

    if [[ -n "${_package_key_url}" ]]; then
        _cur_path=$(pwd)
        _key_path=$(mktemp -d)
        cd "${_key_path}"
        wget -q "${_package_key_url}"
        cd "${_cur_path}"

        for key_file in "${_key_path}/"*.pub; do
            _key_file="${key_file}"
        done

        if [[ -n "${_key_file}" ]]; then
            if [[ ! -f "/etc/signify/$(basename "${_key_file}")" ]]; then
                cp "${_key_file}" /etc/signify
            fi
        fi

        if [[ -d "${_key_path}" ]]; then
            rm -rf "${_key_path}"
        fi
    fi

    print_header "Installing package: '${_package_name}'"
    pkg_add -xvI "${_package_file}"

    if [[ -d "${_package_path}" ]]; then
        rm -rf "${_package_path}"
    fi
}


#####################################################################
# Main script
USE_GITLAB_TOKEN=false
CONFIG_NAME=
CONFIG_ARCHIVE_PATH=
GITLAB_TOKEN=

if [[ $# -lt 1 ]]; then
    printf "\nConfig name is required.\n"
    usage
fi

let PARAM_COUNT=0
while [ $# -gt 0 ]
do
    case "$1" in
        -c) CONFIG_ARCHIVE_PATH="$2"; shift;;
        -g) USE_GITLAB_TOKEN=true;;
        --gitlab_token) USE_GITLAB_TOKEN=true;;
        -*) usage;;
        *) let PARAM_COUNT+=1                   # Handle positional parameters
            case ${PARAM_COUNT} in
                1) CONFIG_NAME=${1};;
                *) ;;
            esac
    esac
    shift
done

if [[ -z "${CONFIG_NAME}" ]]; then
    usage
fi

printf "\n%s\nStarting host configuration with config_host.sh\n" ${SEP}

if [[ -z "${CONFIG_ARCHIVE_PATH}" ]]; then
    # Set a default config path if not specified
    CONFIG_ARCHIVE_PATH="${CONFIG_FILE_PATTERN}"
fi

# Set up paths
CONFIG_PATH="/tmp/${CONFIG_NAME}"
FILES_PATH="${CONFIG_PATH}/files"
CONFIG_FILE="${CONFIG_PATH}/${CONFIG_NAME}.conf"

# Get token up-front
if ${USE_GITLAB_TOKEN}; then
    ask_pass "Enter GitLab token for config files archive: '${CONFIG_PATH}':"
    GITLAB_TOKEN="${resp}"
fi

# Install wget if needed, used to retrieve scripts, config, and patches
if ! /usr/sbin/pkg_info -e net/wget; then
    printf "\n%s\nInstalling wget\n" ${SMSEP}
    /usr/sbin/pkg_add -v wget
fi

# Extract the archive into /tmp/config_name
printf "\n%s\nSearch for config archive\n" ${SMSEP}
get_archive "${CONFIG_ARCHIVE_PATH}" "${CONFIG_NAME}" "/tmp" "${GITLAB_TOKEN}"

if [[ -f "${CONFIG_FILE}" ]]; then
    printf "%s\nReading config file: ${CONFIG_FILE}\n" ${SMSEP};
    # shellcheck disable=SC1090
    . "${CONFIG_FILE}"
else
    echo "Failed to find config file: '${CONFIG_FILE}'"
fi


echo "${SMSEP}"
print_header "Get passwords"

# Read password for root user
ask_password "Enter password for ${CONFIG_NAME} 'root'"
ROOT_PASS=$(encr_pwd "${_password}")


# Read password and SSH public keys for any additional users specified in config
if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
    ADMIN_PASS=
    ADMIN_PUBLIC_KEY=
    let index=0
    while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
        echo ""
        ask_password "Enter password for ${CONFIG_NAME} '${ADMIN_USER[index]}'"
        ADMIN_PASS[index]=$(encr_pwd "${_password}")
        echo ""
        ask_input "Paste public key for ${CONFIG_NAME} '${ADMIN_USER[$index]}' (or blank for none)"
        ADMIN_PUBLIC_KEY[index]="${resp}"
        let index+=1
    done
fi

# Read PSKs to use for replacement in IPSec config if requested
if [[ ${#IPSEC_PSK_NAME[*]} -gt 0 ]]; then
    IPSEC_PSK=
    let index=0
    while [[ $index -lt ${#IPSEC_PSK_NAME[*]} ]]; do
        echo ""
        ask_password "Paste PSK for '${IPSEC_PSK_NAME[$index]}' (or blank for none)"
        IPSEC_PSK[index]="${_password}"
        let index+=1
    done
fi

# Read any additional passwords for replacement, if specified in config
# Each entry represents a PASSWORD_NAME.X to be replaced in one or more files
# PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
# PASSWORD_PROMPT[x]    = Prompt to display when requesting the password from user
# PASSWORD_TYPE[x]      = Password handling type, default/empty is no encryption, E = standard encrypt(1), S = smtpctl(8) encrypt
# PASSWORD_PASS[x]      = Contains password value from user prompt
if [[ ${#PASSWORD_PROMPT[*]} -gt 0 ]]; then
    PASSWORD_PASS=
    let index=0
    while [[ $index -lt ${#PASSWORD_PROMPT[*]} ]]; do
        echo ""
        ask_password "${PASSWORD_PROMPT[$index]}"

        # Default is un-encrypted password
        PASSWORD_PASS[index]="${_password}"
        if [[ ${#PASSWORD_TYPE[*]} -gt 0 ]]; then
            case "${PASSWORD_TYPE[$index]}" in
                E) PASSWORD_PASS[index]=$(encr_pwd "${_password}");;
                S) PASSWORD_PASS[index]=$(echo "${_password}" | smtpctl encrypt);;
            esac
        fi

        let index+=1
    done
fi

_start_time=$(date +%s)
echo ""
echo "${SMSEP}"


if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
    print_header "Admin user setup"
    ADMIN_USER_LIST=""
    let index=0
    while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
        USER_NAME="${ADMIN_USER[$index]}"
        PASS="${ADMIN_PASS[$index]}"
        USER_ID="${ADMIN_ID[$index]}"
        DESC="${ADMIN_DESC[$index]}"
        if [[ -z "${DESC}" ]]; then
            DESC="Local Admin"
        fi
        ADMIN_USER_LIST="${ADMIN_USER_LIST},${USER_NAME}"

        echo "  Adding group for '${USER_NAME}'"
        ADMIN_GROUP="${USER_NAME}:*:${USER_ID}:"
        grep "^${USER_NAME}:" /etc/group >/dev/null || echo "${ADMIN_GROUP}" >> /etc/group

        echo "  Adding user password entry for '${USER_NAME}'"
        ADMIN_LINE="${USER_NAME}:${PASS}:${USER_ID}:${USER_ID}:staff:0:0:${DESC}:/home/${USER_NAME}:/bin/ksh"
        grep "^${USER_NAME}:" /etc/master.passwd >/dev/null || echo "$ADMIN_LINE" >> /etc/master.passwd

        let index+=1
        echo ""
    done

    echo "Adding admin users to wheel group"
    sed -i -e "s@^wheel:.:0:root\$@wheel:\*:0:root${ADMIN_USER_LIST}@" /etc/group 2>/dev/null
fi


if [[ ${#NO_PW_USER[*]} -gt 0 ]]; then
    print_header "No-password user setup"
    let index=0
    while [[ $index -lt ${#NO_PW_USER[*]} ]]; do
        USER_NAME="${NO_PW_USER[$index]}"
        PASS="*"
        USER_ID="${NO_PW_USER_ID[$index]}"
        USER_HOME="${NO_PW_USER_HOME[$index]}"
        USER_SHELL="${NO_PW_USER_SHELL[$index]}"
        DESC="${NO_PW_USER_DESC[$index]}"
        if [[ -z "${DESC}" ]]; then
            DESC="${USER_NAME}"
        fi

        echo "  Adding group for '${USER_NAME}'"
        NO_PW_GROUP="${USER_NAME}:*:${USER_ID}:"
        grep "^${USER_NAME}:" /etc/group >/dev/null || echo "${NO_PW_GROUP}" >> /etc/group

        echo "  Adding user entry for '${USER_NAME}'"
        NO_PW_LINE="${USER_NAME}:${PASS}:${USER_ID}:${USER_ID}::0:0:${DESC}:${USER_HOME}:${USER_SHELL}"
        grep "^${USER_NAME}:" /etc/master.passwd >/dev/null || echo "$NO_PW_LINE" >> /etc/master.passwd

        let index+=1
        echo ""
    done
fi

print_header "root user setup"
if [[ -n "${CHANGE_ROOT_NAME_TO_HOSTNAME}" && "${CHANGE_ROOT_NAME_TO_HOSTNAME}" == "yes" ]]; then
    ROOT_NAME=$(capitalize "$(echo "${MY_NAME}" | cut -d "." -f1)")
    echo "  Changing root name to '${ROOT_NAME}'"
    sed -i -e "s@Charlie@${ROOT_NAME}@" /etc/master.passwd 2>/dev/null
fi

echo "  Setting man to not clear screen after exiting 'root'"
echo "export MANPAGER=\"less -X\"" >> "/root/.profile"
chown root:wheel "/root/.profile"

echo "  Setting vi tab stops for 'root'"
printf ":set expandtab\n:set shiftwidth=4\n:set tabstop=4\n" > "/root/.nexrc"
chown root:wheel "/root/.nexrc"
chmod go-wx "/root/.nexrc"

echo ""
echo "  Setting root password"
sed -i -e "s@^root:[^:]*:@root:${ROOT_PASS}:@" /etc/master.passwd 2>/dev/null

print_header "Updating password DB"
pwd_mkdb -p -d /etc /etc/master.passwd

if [[ ${#NO_PW_USER[*]} -gt 0 && ${#NO_PW_USER_GROUPS[*]} -gt 0 ]]; then
    # To use usermod, this section must come after pwd_mkdb
    print_header "Updating No-password user group membership"
    let index=0
    while [[ $index -lt ${#NO_PW_USER[*]} ]]; do
        USER_NAME="${NO_PW_USER[$index]}"
        local _user_groups="${NO_PW_USER_GROUPS[$index]}"
        if [[ -n "${_user_groups}" ]]; then
            # This will need to be chrooted in config_image
            echo "  Assigning secondary group(s) for '${USER_NAME}'"
            /usr/sbin/usermod -G "${_user_groups}" "${USER_NAME}"
            echo ""
        fi

        let index+=1
    done
fi

if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
    print_header "Setup admin users"

    let index=0
    while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
        USER_NAME="${ADMIN_USER[$index]}"
        USER_ID="${ADMIN_ID[$index]}"

        echo "  Adding home dir for '${USER_NAME}'"
        mkdir -p "/home/${USER_NAME}"
        chown "${USER_ID}":"${USER_ID}" "/home/${USER_NAME}"

        echo "  Setting man to not clear screen after exiting '${USER_NAME}'"
        cp "/etc/skel/.profile" "/home/${USER_NAME}/.profile"
        echo "export MANPAGER=\"less -X\"" >> "/home/${USER_NAME}/.profile"
        chown "${USER_ID}":"${USER_ID}" "/home/${USER_NAME}/.profile"

        echo "  Setting vi tab stops for '${USER_NAME}'"
        printf ":set expandtab\n:set shiftwidth=4\n:set tabstop=4\n" > "/home/${USER_NAME}/.nexrc"
        chown "${USER_ID}":"${USER_ID}" "/home/${USER_NAME}/.nexrc"
        chmod go-wx "/home/${USER_NAME}/.nexrc"

        if [[ ${#ADMIN_PUBLIC_KEY[*]} -gt 0 ]]; then
            KEY="${ADMIN_PUBLIC_KEY[$index]}"
            echo "  Adding public key for '${USER_NAME}'"
            mkdir -p "/home/${USER_NAME}/.ssh"
            touch "/home/${USER_NAME}/.ssh/authorized_keys"
            echo "${KEY}" >> "/home/${USER_NAME}/.ssh/authorized_keys"

            chown -R "${USER_ID}":"${USER_ID}" "/home/${USER_NAME}/.ssh"
            chmod go-rwx "/home/${USER_NAME}/.ssh"
            chmod go-rwx "/home/${USER_NAME}/.ssh/authorized_keys"
        else
            echo " Skipping ${USER_NAME} public key"
        fi

        let index+=1
        echo ""
    done
fi

echo ""
echo "${SMSEP}"

print_header "Doas setup"
if [[ -f "${FILES_PATH}/doas.conf" ]]; then
    echo "  Copying custom doas.conf file"
    cp "${FILES_PATH}/doas.conf" "/etc/doas.conf"
else
    echo "  Copying example doas.conf file"
    cp "/etc/examples/doas.conf" "/etc/doas.conf"
    sed -i -e "s@^permit keepenv :wheel\$@permit persist keepenv :wheel@" /etc/doas.conf 2>/dev/null
fi
chmod 0600 "/etc/doas.conf"


# ADD_RO_RW_SCRIPTS and ADD_RO_RW_TO_WEEKLY are ignored in config_host.sh


print_header "SSH setup"
if [[ -n "${SSH_BANNER}" ]]; then
    echo "  Adding SSH banner content to ssh_banner"
    echo "${SSH_BANNER}" >> /etc/ssh/ssh_banner
else
    if [[ -f "${FILES_PATH}/ssh_banner" ]]; then
        echo "  Copying custom ssh_banner file"
        cp "${FILES_PATH}/ssh_banner" "/etc/ssh/ssh_banner"
    fi
fi

if [[ -n "${SSHD_CONFIG}" ]]; then
    echo "  Addng SSH config content to sshd_config"
    echo "${SSHD_CONFIG}" >> /etc/ssh/sshd_config
else
    if [[ -f "${FILES_PATH}/sshd_config" ]]; then
        echo "  Copying custom sshd_config file"
        cp "${FILES_PATH}/sshd_config" "/etc/ssh/sshd_config"
    fi
fi


print_header "Network setup"
if [[ -n "${MY_GATE}" ]]; then
    echo "  Adding gateway"
    echo "${MY_GATE}" > /etc/mygate
fi

if [[ -n "${MY_NAME}" ]]; then
    echo "  Adding myname"
    echo "${MY_NAME}" > /etc/myname
fi

if [[ ${#INTERFACE[*]} -gt 0 ]]; then
    # This does not need to be reversed because these are file names
    let index=0
    while [[ $index -lt ${#INTERFACE[*]} ]]; do
        copy_file "${FILES_PATH}/hostname.${index}" "/etc/hostname.${INTERFACE[$index]}" "0640" "root:wheel"
        let index+=1
    done
else
    for HOSTNAME_FILE in "${FILES_PATH}/hostname."*
    do
        DST="/etc/${HOSTNAME_FILE##*/}"
        echo "  Copying ${HOSTNAME_FILE##*/}"
        cp "${HOSTNAME_FILE}" "${DST}"
        chmod 0640 "${DST}"
    done
fi


if [[ -f "${FILES_PATH}/dhcpd.conf" ]]; then
    echo "  Copying dhcpd.conf"
    cp "${FILES_PATH}/dhcpd.conf" "/etc/dhcpd.conf"
fi


if [[ -f "${FILES_PATH}/hosts" ]]; then
    echo "  Copying hosts"
    cp "${FILES_PATH}/hosts" "/etc/hosts"
else
    if [[ ! -f "/etc/hosts" ]]; then
        echo "  Creating default hosts file"
        printf "127.0.0.1       localhost\n::1             localhost\n" >> /etc/hosts
    fi
fi


if [[ -n "${ISAKMPD_POLICY}" ]]; then
    echo "  Adding isakmpd policy"
    echo "${ISAKMPD_POLICY}" >> /etc/isakmpd/isakmpd.policy
    chmod 0600 /etc/isakmpd/isakmpd.policy
else
    if [[ -f "${FILES_PATH}/isakmpd.policy" ]]; then
        echo "  Copying isakmpd.policy"
        cp "${FILES_PATH}/isakmpd.policy" "/etc/isakmpd/isakmpd.policy"
        chmod 0600 /etc/isakmpd/isakmpd.policy
    fi
fi


if [[ -n "${IPSEC_FILE_PATTERN}" ]]; then
    for IPSEC_CONFIG in "${FILES_PATH}"/${IPSEC_FILE_PATTERN}
    do
        echo "  Copying ${IPSEC_CONFIG##*/}"
        DST="/etc/${IPSEC_CONFIG##*/}"
        cp "${IPSEC_CONFIG}" "${DST}"
        replace_psk_place_holders "${DST}"
        chmod 0600 "/etc/${IPSEC_CONFIG##*/}"
    done
fi


if [[ -f "${FILES_PATH}/pf.conf" ]]; then
    copy_file "${FILES_PATH}/pf.conf" "/etc/pf.conf" "0600" "root:wheel"
fi


if [[ -n "${NTP_CONF}" ]]; then
    echo "  Adding custom NTP config to ntpd.conf"
    echo "${NTP_CONF}" >> "/etc/ntpd.conf"
else
    if [[ -f "${FILES_PATH}/ntpd.conf" ]]; then
        echo "  Copying custom ntpd.conf file"
        cp "${FILES_PATH}/ntpd.conf" "/etc/ntpd.conf"
    fi
fi

if [[ ${#ADDL_PACKAGES[*]} -gt 0 ]]; then
    printf "\n%s\n" "${SMSEP}"
    echo "Installing additional packages"
    echo "${SMSEP}"
    let index=0
    while [[ $index -lt ${#ADDL_PACKAGES[*]} ]]; do
        if [[ "${ADDL_PACKAGES[$index]}" == *\|* ]]; then
            install_package_by_url "${ADDL_PACKAGES[$index]}"
        else
            print_header "Installing package: '${ADDL_PACKAGES[$index]}'"
            pkg_add -xvI "${ADDL_PACKAGES[$index]}"
        fi

        let index+=1
    done
    echo "${SMSEP}"
    echo ""
fi


print_header "rc.conf.local setup"
if [[ -n "${RC_CONF_LOCAL}" ]]; then
    echo "  Adding config to rc.conf.local"
    echo "${RC_CONF_LOCAL}" >> "/etc/rc.conf.local"
    replace_interface "/etc/rc.conf.local"
else
    if [[ -f "${FILES_PATH}/rc.conf.local" ]]; then
        echo "  Copying custom rc.conf.local file"
        cp "${FILES_PATH}/rc.conf.local" "/etc/rc.conf.local"
        replace_interface "/etc/rc.conf.local"
    else
        echo "  No rc.conf.local setup required"
    fi
fi


print_header "rc.local setup"
if [[ -n "${RC_LOCAL}" ]]; then
    echo "  Adding config to rc.local"
    echo "${RC_LOCAL}" >> "/etc/rc.local"
    replace_interface "/etc/rc.local"
else
    if [[ -f "${FILES_PATH}/rc.local" ]]; then
        echo "  Copying custom rc.local file"
        cp "${FILES_PATH}/rc.local" "/etc/rc.local"
        replace_interface "/etc/rc.local"
    else
        echo "  No rc.local setup required"
    fi
fi


print_header "sysctl.conf setup"
if [[ -f "${FILES_PATH}/sysctl.conf" ]]; then
    echo "  Copying custom sysctl.conf file"
    cp "${FILES_PATH}/sysctl.conf" "/etc/sysctl.conf"
else
    echo "  Copying default sysctl.conf"
    cp "/etc/examples/sysctl.conf" "/etc/sysctl.conf"
fi

if [[ -n "${SYSCTL_ENABLE_IPV4_FORWARDING}" && "${SYSCTL_ENABLE_IPV4_FORWARDING}" == "yes" ]]; then
    echo "Enabling net.inet.ip.forwarding in sysctl.conf"
    sed -i -e "s@^#net.inet.ip.forwarding=[01]@net.inet.ip.forwarding=1@" /etc/sysctl.conf 2>/dev/null
else
    echo "Disabling net.inet.ip.forwarding in sysctl.conf"
    sed -i -e "s@^#net.inet.ip.forwarding=[01]@net.inet.ip.forwarding=0@" /etc/sysctl.conf 2>/dev/null
fi

if [[ -n "${SYSCTL_ENABLE_IPV6_FORWARDING}" && "${SYSCTL_ENABLE_IPV6_FORWARDING}" == "yes" ]]; then
    echo "Enabling net.inet6.ip6.forwarding in sysctl.conf"
    sed -i -e "s@^#net.inet6.ip6.forwarding=[01]@net.inet6.ip6.forwarding=1@" /etc/sysctl.conf 2>/dev/null
else
    echo "Disalbing net.inet6.ip6.forwarding in sysctl.conf"
    sed -i -e "s@^#net.inet6.ip6.forwarding=[01]@net.inet6.ip6.forwarding=0@" /etc/sysctl.conf 2>/dev/null
fi


if [[ -n "${CONFIGURE_UNBOUND}" && "${CONFIGURE_UNBOUND}" == "yes" ]]; then
    print_header "Configuring Unbound"
    LOCAL_ROOT_HINT_PATH="/tmp/root.hints"
    echo "Retrieving current root hints"
    wget "${ROOT_HINTS_URL}" -O "${LOCAL_ROOT_HINT_PATH}"


    if [[ -z "${UNBOUND_CONF}" ]]; then
        # Set default file name if none specified
        UNBOUND_CONF="unbound.conf"
    fi

    if [[ -f "${FILES_PATH}/${UNBOUND_CONF}" ]]; then
        echo "Copying ${UNBOUND_CONF} to default unbound config path: /var/unbound/etc/unbound.conf"
        DST="/var/unbound/etc/unbound.conf"
        cp "${FILES_PATH}/${UNBOUND_CONF}" "${DST}"
        replace_interface "${DST}"
    else
        echo "Using default unbound.conf. This may NOT be what you want."
    fi

    if [[ -f "${LOCAL_ROOT_HINT_PATH}" ]]; then
        echo "Copying root hints"
        cp "${LOCAL_ROOT_HINT_PATH}" "/var/unbound/etc/root.hints"
    else
        # Should not be needed, but just in case
        echo "Retrieving current root hints"
        wget "${ROOT_HINTS_URL}" -O "/var/unbound/etc/root.hints"
    fi

    echo "Running unbound-anchor to update the root trust anchor for DNSSEC validation"
    unbound-anchor -a "/var/unbound/db/root.key" -r "/var/unbound/etc/root.hints"
fi

echo ""
echo "${SMSEP}"

print_header "Additional files"
copy_files "/etc" "${MISC_ETC_FILE[*]}" "${MISC_ETC_PERMS[*]}" "${MISC_ETC_OWNER[*]}"
echo "${SMSEP}"
copy_files "" "${MISC_MISC_FILE[*]}" "${MISC_MISC_PERMS[*]}" "${MISC_MISC_OWNER[*]}"

if [[ ${#SCRIPT_FILE[*]} -gt 0 && -n "${SCRIPT_PATH}" ]]; then
    print_header "Additional scripts"
    if ! [[ -d "${SCRIPT_PATH}" ]] ; then
        echo "Creating script path: ${SCRIPT_PATH}"
        mkdir -p "${SCRIPT_PATH}"
    fi

    echo "${SMSEP}"
    copy_files "${SCRIPT_PATH}" "${SCRIPT_FILE[*]}" "${SCRIPT_PERMS[*]}" "${SCRIPT_OWNER[*]}"
fi
echo "${SMSEP}"

if [[ -n "${ADDL_CRON_PATH}" && -f "${FILES_PATH}/${ADDL_CRON_PATH}" ]]; then
    print_header "Adding root crontab entries from '${ADDL_CRON_PATH}'"
    crontab -l -u root | cat - "${FILES_PATH}/${ADDL_CRON_PATH}" | crontab -u root -
fi

if [[ ${#PASSWORD_FILE[*]} -gt 0 && ${#PASSWORD_PASS[*]} -gt 0 ]]; then
    print_header "Replacing passwords"
    replace_all_password_place_holders
fi

if [[ -n "${TIMEZONE_PATH}" ]]; then
    print_header "Timezone setup"
    echo "Setting timezone to ${TIMEZONE_PATH}"
    ln -fs "/usr/share/zoneinfo/${TIMEZONE_PATH}" "/etc/localtime"
fi

# Post-install scripts
if [[ ${#POST_INSTALL_ACTION[*]} -gt 0 ]]; then
    printf "\n%s\n" "${SMSEP}"
    echo "Running post-install scripts"
    echo "${SMSEP}"

    let index=0
    while [[ $index -lt ${#POST_INSTALL_ACTION[*]} ]]; do
        desc="Running post-install action #${index}"
        if [[ -n "${POST_INSTALL_SCRIPT[$index]}" ]]; then
                desc="Running '/tmp/$(basename "${POST_INSTALL_SCRIPT[$index]}")'"
                if [[ -n "${POST_INSTALL_DESC[$index]}" ]]; then
                    desc="${POST_INSTALL_DESC[$index]}"
                fi
        else
            if [[ -n "${POST_INSTALL_ACTION[$index]}" ]]; then
                if [[ -n "${POST_INSTALL_DESC[$index]}" ]]; then
                    desc="${POST_INSTALL_DESC[$index]}"
                fi
            fi
        fi

        # The "echo" is to make sure that ANSI escape codes get passed to print_header
        # Need to avoid double-quoting the description
        # shellcheck disable=SC2086
        print_header "$(echo -e ${desc})"

        if [[ -n "${POST_INSTALL_SCRIPT[$index]}" ]]; then
            copy_file "${FILES_PATH}/${POST_INSTALL_SCRIPT[$index]}" "/tmp/${POST_INSTALL_SCRIPT[$index]}" "0755" "root:wheel"
            replace_password_place_holders "/tmp/${POST_INSTALL_SCRIPT[$index]}"
        fi

        if [[ -n "${POST_INSTALL_ACTION[$index]}" ]]; then
            eval "${POST_INSTALL_ACTION[$index]}"
        else
            if [[ -n "${POST_INSTALL_SCRIPT[$index]}" ]]; then
                eval "/tmp/${POST_INSTALL_SCRIPT[$index]}"
            fi
        fi
        let index+=1
    done
else
    let index=0
    if [[ ${#POST_INSTALL_SCRIPT[*]} -gt 0 ]]; then
        printf "\n%s\n" "${SMSEP}"
        echo "Running post-install scripts"
        echo "${SMSEP}"

        while [[ $index -lt ${#POST_INSTALL_SCRIPT[*]} ]]; do
            if [[ -n "${POST_INSTALL_SCRIPT[$index]}" ]]; then
                desc="Running '/tmp/$(basename "${POST_INSTALL_SCRIPT[$index]}")'"
                if [[ -n "${POST_INSTALL_DESC[$index]}" ]]; then
                    desc="${POST_INSTALL_DESC[$index]}"
                fi

                # The "echo" is to make sure that ANSI escape codes get passed to print_header
                # Need to avoid double-quoting the description
                # shellcheck disable=SC2086
                print_header "$(echo -e ${desc})"

                copy_file "${FILES_PATH}/${POST_INSTALL_SCRIPT[$index]}" "/tmp/${POST_INSTALL_SCRIPT[$index]}" "0755" "root:wheel"
                replace_password_place_holders "/tmp/${POST_INSTALL_SCRIPT[$index]}"
                eval "/tmp/${POST_INSTALL_SCRIPT[$index]}"
            fi
            let index+=1
        done
    fi
fi

# End recording time
printf "\n\n%s\nHost configuration completed with config_host.sh\n Elapsed time: %ds\n%s\n" ${SMSEP} $(($(date +%s) - _start_time)) ${SEP}
