#!/bin/sh
#
# Configure resflash images (.img or .fs)
# Automate configuration to reduce effort to create images
# Mounts image file (.img or .fs), configures, and unmounts
#
# Intended to be called from build_premade_auto.sh, a patched
#   resflash-tools/build_premade.sh
#   https://gitlab.com/bconway/resflash-tools/raw/master/build_premade.sh
#
# Configuration based on files in: /imaging/config/<config_name>
#
# Configuration steps:
#   - Gather user passwords, private keys, IPSec PSKs,
#   - Loop over images, mounting and configuring
#   - Add admin users (ADMIN_USER, ADMIN_ID)
#   - Change root user name if requested (CHANGE_ROOT_NAME_TO_HOSTNAME)
#   - Set root password
#   - Update password database
#   - Add admin user public keys (authorized_keys)
#   - Copy files/doas.conf or copy from /etc/examples
#   - Add ro/rw scripts if requested (ADD_RO_RW_SCRIPTS)
#       Modify /etc/weekly with ro/rw if requested (ADD_RO_RW_TO_WEEKLY)
#   - Copy/create ssh_banner, sshd_config (SSH_BANNER, SSHD_CONFIG)
#   - Create /etc/mygate (MY_GATE)
#   - Create /etc/myname (MY_NAME)
#   - Copy files/hostname.*
#       Rename hostname.* files with correct interface name if INTERFACE is defined
#       Replace interface place-holders if INTERFACE is defined
#   - Copy files/dhcpd.conf
#   - Copy files/hosts or create default hosts file with localhost
#   - Copy/create /etc/isakmpd/isakmpd.policy file (ISAKMPD_POLICY)
#   - Copy IPSec config files (IPSEC_FILE_PATTERN)
#       Replace PSK place-holders if IPSEC_PSK_NAME defined
#   - Copy pf.conf
#       Replace interface place-holders if INTERFACE is defined
#   - Copy/create /etc/ntp.conf (NTP_CONF)
#   - Copy/create /etc/rc.conf.local (RC_CONF_LOCAL)
#   - Copy/create /etc/rc.local (RC_LOCAL)
#   - Copy/create /etc/sysctl.conf (SYSCTL_ENABLE_IPV4_FORWARDING, SYSCTL_ENABLE_IPV6_FORWARDING)
#   - Copy/create unbound configuration (CONFIGURE_UNBOUND, UNBOUND_CONF)
#       Replace interface place-holders if INTERFACE is defined in unbound.conf
#       Retrieves current root hints file
#       Runs unbound-anchor to set the root trust anchor for DNSSEC validation
#       You are responsible for enabling unbound in /etc/rc.conf.local, configuring resolv.conf, and
#       any necessary changes to dhcpd.conf
#   - Copy additional files to (MISC_ETC_FILE, MISC_MISC_FILE)
#   - Copy additional script files to SCRIPT_PATH and apply SCRIPT_PERMS permissions (SCRIPT_FILE)
#   - Add additional root crontab entries (ADDL_CRON_PATH)
#   - Replace passwords in files if specified (PASSWORD_FILE, PASSWORD_PROMPT)
#   - Set timezone if specified (TIMEZONE_PATH)
#
# Image config file:
#   /imaging/config/<config_name>/<config_name>.conf
#   Sets configuration items for the specific configuration instance
#
# Directory structure:
#   /imaging/config/<config_name>/files
#       ... any config files to be copied ...
#
# usage:   ./config_image.sh <config_name> <path_to_images> <path_to_resflash>
# Example: ./config_image.sh catnip /imaging/images/catnip/resflash-amd64- /imaging/resflash
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
CONFIG_ROOT="${ROOT_DIR}/config"
ROOT_HINTS_URL="http://www.internic.net/domain/named.root"

SEP="=============================================================================="
SMSEP="-----------------------------------------------------------------"
TSEP="+++++"


#####################################################################
# Password methods borrowed from OpenBSD miniroot
#####################################################################
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
#
ask_pass() {
        stty -echo
        IFS= read -r resp?"$1 "
        stty echo
        echo
}


# Ask for a password twice, saving the input in $_password.
ask_password() {
        local _q=$1

        while :; do
                ask_pass "$_q (will not echo)"
                # $resp is assigned by ask_pass, disable shellcheck warning
                # shellcheck disable=SC2154
                _password=$resp

                ask_pass "$_q (again)"
                [[ $resp == "$_password" ]] && break

                echo "Passwords do not match, try again."
        done
}


# Ask for input twice, saving input in $resp, without disabling echo
ask_input() {
    local _q=$1
    local _temp_resp

    while :; do
        read -r resp?"$_q "
        _temp_resp=$resp

        read -r resp?"$_q (again) "
        [[ $resp == "$_temp_resp" ]] && break

        echo "Values do not match, try again."
    done
}


encr_pwd() {
        local _p=$1

        if [[ -z $_p ]]; then
                echo '*'
        elif [[ $_p == \$2?\$[0-9][0-9]\$* && ${#_p} -gt 40 ||
                $_p == '*************' ]]; then
                echo "$_p"
        else
                encrypt -b a -- "$_p"
        fi
}


usage() {
    printf >&2 "Usage: %s config_name image_path resflash_path\n       config_name                         Name of configuration name used to find configuration file(s)\n       image_path                          Partial path to image being configured (e.g., /imaging/images/config_name/resflash-amd64-)\n       resflash_path                       Path to resflash installation (e.g. /imaging/resflash)\nExample: %s example /imaging/images/example/resflash-amd64- /imaging/resflash\n\n" "${0}" "${0}"
    exit 1
}


# Print a header using TSEP
# $1 = header message
print_header() {
    printf "\n%s %s %s\n" ${TSEP} "$1" ${TSEP}
}


capitalize() {
  typeset -u first
  first=${1%"${1#?}"}
  print "${first}${1#?}"
}


# Replace interface name place-holders with interface name defined in INTERFACE array
# Pass file name to replace as first argument
replace_interface() {
    local _FILE_NAME=$1
    local _local_index
    local _file_type

    if [[ ${#INTERFACE[*]} -gt 0 ]]; then
        _file_type=$(file -b "${_FILE_NAME}")
        if [ "${_file_type##*text*}" ]; then
            echo "Skipping interface replacement in non-text file: '${_FILE_NAME}'"
        else
            echo "Replacing interface place-holders in ${_FILE_NAME##*/}"
            # Reverse array order to prevent single-digit indexes from prematurely
            # replacing double-digit indexes (i.e., '1' replacing '10')
            let _local_index=${#INTERFACE[*]}
            while [[ $_local_index -gt 0 ]]; do
                let _local_index-=1
                # Replace INTERFACE.$_local_index with ${INTERFACE[$_local_index]}
                local SRC="INTERFACE.${_local_index}"
                local DST="${INTERFACE[$_local_index]}"
                sed -i -e "s@$SRC@$DST@g" "${_FILE_NAME}" 2>/dev/null
            done
        fi
    fi
}


# Replace psk name place-holders with user-specified values
# Pass file name to replace as first argument
replace_psk_place_holders() {
    local _FILE_NAME=$1
    local _local_index
    if [[ ${#IPSEC_PSK_NAME[*]} -gt 0 && ${#IPSEC_PSK[*]} -gt 0 ]]; then
        echo "Replacing PSK place-holders in '${_FILE_NAME##*/}'"
        let _local_index=0
        while [[ $_local_index -lt ${#IPSEC_PSK_NAME[*]} ]]; do
            local SRC="${IPSEC_PSK_NAME[$_local_index]}"
            local DST="${IPSEC_PSK[$_local_index]}"
            sed -i -e "s@$SRC@$DST@g" "${_FILE_NAME}" 2>/dev/null
            let _local_index+=1
        done
    fi
}


# Replace all password placeholders specified in PASSWORD_FILE files
# Each entry represents a PASSWORD_NAME.X to be replaced in one or more files
# PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
# PASSWORD_PASS[x]      = Contains password value from user prompt
replace_all_password_place_holders() {
    local _cur_file
    local _cur_files
    local _all_files
    local _unique_files

    # Flatten list of files
    if [[ ${#PASSWORD_FILE[*]} -gt 0 && ${#PASSWORD_PASS[*]} -gt 0 ]]; then
        local _file
        local _index
        let _index=0
        for _file in "${PASSWORD_FILE[@]}"
        do
            OLD_IFS=$IFS
            IFS="|"
            # This is an array and can't be quoted
            # shellcheck disable=SC2086
            set -A _cur_files ${_file}
            IFS=$OLD_IFS
            for _cur_file in "${_cur_files[@]}"
            do
                _all_files[_index]=$_cur_file
                let _index+=1
            done
        done

        # Get list of unique files
        NL=$(printf '\n.')
        NL=${NL%?}
        OLD_IFS=$IFS
        IFS=$NL
        # This is an array and can't be quoted
        # shellcheck disable=SC2046
        set -A _unique_files $( for _cur_file in "${_all_files[@]}"; do printf "%s\n" "${_cur_file}"; done | sort -u )
        IFS=$OLD_IFS

        # Replace placeholders in list of unique files
        for _file in "${_unique_files[@]}"
        do
            replace_password_place_holders "${_file}"
        done
    fi
}


# Replace password tokens in file
# Each entry represents a PASSWORD_NAME.X (e.g., PASSWORD_NAME.0, PASSWORD_NAME.1, etc) to be replaced in one or more files
# PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
# PASSWORD_PASS[x]      = Contains password value from user prompt
#
# $1 = File name (full path) be replaced
replace_password_place_holders() {
    local _FILE_NAME="${FS_PATH}${1}"
    local _temp_file_pass
    local _temp_file_out
    local _file_index
    local _file_mode
    local _file_owner
    local _file_type

    if [[ -f "${_FILE_NAME}" ]]; then
        _file_type=$(file -b "${_FILE_NAME}")
        if [ "${_file_type##*text*}" ]; then
            echo "Skipping password replacement in non-text file: '${_FILE_NAME}'"
        else
            echo "Replacing password place holders in '${_FILE_NAME}'"
            _temp_file_out=$(mktemp)
            # Copy should change ownership to current user, but not file permissions
            cp "${_FILE_NAME}" "${_temp_file_out}"
            _file_mode=$(stat -f "%#OLp" "${_FILE_NAME}")
            _file_owner=$(stat -f "%Su:%Sg" "${_FILE_NAME}")

            if [[ $(echo "${_file_mode}" | cut -b 2) -lt 6 ]]; then
                # Grant write permissions to current user
                chmod 0600 "${_temp_file_out}"
            fi

            # Reverse array order to prevent single-digit indexes from prematurely
            # replacing double-digit indexes (i.e., '1' replacing '10')
            let _file_index=${#PASSWORD_FILE[*]}
            while [[ $_file_index -gt 0 ]]; do
                let _file_index-=1
                local _cur_files
                local _cur_file
                local _temp_file_pass

                OLD_IFS=$IFS
                IFS="|"
                # This is an array and can't be quoted
                # shellcheck disable=SC2086
                set -A _cur_files ${PASSWORD_FILE[$_file_index]}
                IFS=$OLD_IFS

                for _cur_file in "${_cur_files[@]}"
                do
                    if [[ "${_FILE_NAME}" == "${FS_PATH}${_cur_file}" ]]; then
                        # Write password value to tempfile, escaping ampersands which cause issues in awk replacement
                        _temp_file_pass=$(mktemp)
                        printf "%s\n" "${PASSWORD_PASS[$_file_index]}" | perl -pe 's/&/\\&/g' >> "${_temp_file_pass}"

                        # sed can't be used to do this because we can't know
                        # if there are any delimiters in the password itself
                        lineCount=$(printf "%s" "$(wc -l "${_temp_file_out}")" | sed 's/^[ \t]*//;s/[ \t]*$//' | cut -d' ' -f 1)
                        hasTrailingNewLine=$(printf "%s" "$(tail -c1 "${_temp_file_out}" | wc -l)" | sed 's/^[ \t]*//;s/[ \t]*$//')

                        awk -F '\n' -v pattern="PASSWORD_NAME\.${_file_index}" -v lineCount="${lineCount}" -v hasTrailingNewLine="${hasTrailingNewLine}" '
                            BEGIN { OFS = FS }
                            NR == FNR { password = $0; next }
                            {
                                if (match($0, pattern) > 0)
                                {
                                    gsub(pattern, password);
                                }
                                printf("%s", $0);
                                isLastLine = (NR > lineCount);
                                if (!isLastLine || (isLastLine && hasTrailingNewLine > 0))
                                {
                                    printf("\n");
                                }
                            }' "${_temp_file_pass}" "${_temp_file_out}" > "${_temp_file_out}.new" && mv "${_temp_file_out}.new" "${_temp_file_out}"

                        # Delete temp file
                        rm -f "${_temp_file_pass}"
                    fi
                done
            done

            mv "${_temp_file_out}" "${_FILE_NAME}"
            if [[ "$(stat -f "%#OLp" "${_FILE_NAME}")" != "${_file_mode}" ]] ; then
                chmod "${_file_mode}" "${_FILE_NAME}"
            fi

            if [[ "$(stat -f "%Su:%Sg" "${_FILE_NAME}")" != "${_file_owner}" ]] ; then
                chown "${_file_owner}" "${_FILE_NAME}"
            fi
        fi
    fi
}


# Copy configuration files, setting permissions and ownership
# $1 = destination path (no trailing slash)
# $2 = list of file names to copy from $FILES_PATH
# $3 = list of associated file permissions to apply (if any)
# $4 = list of associated file owners to apply (if any)
copy_files() {
    local _dest=$1
    local _fileNames
    local _filePerms
    local _fileOwners

    # Note: set -A will lose any null/blank array entries. This will break the "shared"
    # index between _fileNames, _filePerms, & _fileOwners.
    # This makes all _filePerms & _fileOwners entries required and non-empty/null
    # These argument variables are arrays, so they can't be quoted or they will not assign properly
    # shellcheck disable=SC2086
    set -A _fileNames $2
    # shellcheck disable=SC2086
    set -A _filePerms $3
    # shellcheck disable=SC2086
    set -A _fileOwners $4

    if [[ ${#_fileNames[*]} -gt 0 ]]; then
        local _index
        let _index=0
        while [[ $_index -lt ${#_fileNames[*]} ]]; do
            local _filePerm=""
            if [[ ${#_filePerms[*]} -gt 0 ]]; then
                _filePerm="${_filePerms[$_index]}"
            fi

            local _fileOwner=""
            if [[ ${#_fileOwners[*]} -gt 0 ]]; then
                _fileOwner="${_fileOwners[$_index]}"
            fi

            copy_file "${FILES_PATH}/${_fileNames[$_index]}" "${_dest}/${_fileNames[$_index]}" "${_filePerm}" "${_fileOwner}"
            let _index+=1
        done
    fi
}


# Copy a configuration file, setting permissions and ownership
# $1 = source file name (full path)
# $2 = destination file name (full path)
# $3 = file permission to apply (if any)
# $4 = file owner to set (if any)
copy_file() {
    local _fileName=$1
    local _destFileName=$2
    if [ "${_destFileName}" == "${_destFileName#/}" ]; then
        _destFileName="/${_destFileName}"
    fi
    local _destDirName
    _destDirName=$(dirname "${_destFileName}")
    local _filePerm=$3
    local _fileOwner=$4

    if [[ -f "${_fileName}" ]]; then
        if [[ -n "${_destFileName}" ]]; then
            if [[ ! -d "${_destDirName}" ]]; then
                echo "Creating path '${_destDirName}'"
                mkdir -p "${_destDirName}"
            fi

            echo "Copying '$(basename "${_fileName}")'"
            cp "${_fileName}" "${_destFileName}"
            replace_interface "${_destFileName}"

            if [[ -n "${_filePerm}" ]]; then
                echo "Setting permissions '${_filePerm}' on ${_destFileName}"
                chmod "${_filePerm}" "${_destFileName}"
            fi

            if [[ -n "${_fileOwner}" ]]; then
                echo "Setting owner '${_fileOwner}' on ${_destFileName}"
                chown "${_fileOwner}" "${_destFileName}"
            fi

            echo ""
        fi
    else
        printf '\033[1mWARNING: File not copied. Source file not found for '\''%s'\''\033[0m\n' "${_fileName}"
    fi
}


#####################################################################
# Main script

if [[ $# -gt 1 ]]; then
    RESFLASH_PATH=
    let PARAM_COUNT=0
    while [ $# -gt 0 ]
    do
        case "$1" in
            -*) usage;;
            *) let PARAM_COUNT+=1                   # Handle positional parameters
                case ${PARAM_COUNT} in
                    1) CONFIG_NAME=${1};;
                    2) IMAGE_PATH=${1};;
                    3) RESFLASH_PATH=${1};;
                    *) ;;
                esac
        esac
        shift
    done

    if [[ -z "${CONFIG_NAME}" || -z "${IMAGE_PATH}" || -z "${RESFLASH_PATH}" ]]; then
        usage
    fi


    printf "\n%s\nStarting '%s' image configuration with config_image.sh\n" ${SEP} "${CONFIG_NAME}"

    # Set up paths
    CONFIG_PATH="${CONFIG_ROOT}/${CONFIG_NAME}"
    FILES_PATH="${CONFIG_PATH}/files"
    CONFIG_FILE="${CONFIG_PATH}/${CONFIG_NAME}.conf"

    if [[ -f "${CONFIG_FILE}" ]]; then
        printf "%s\nReading config file: %s\n" ${SMSEP} "${CONFIG_FILE}";
        # shellcheck disable=SC1090
        . "${CONFIG_FILE}"
    fi


    echo "${SMSEP}"
    print_header "Get passwords"

    # Read password for root user
    ask_password "Enter password for ${CONFIG_NAME} 'root'"
    ROOT_PASS=$(encr_pwd "${_password}")


    # Read password and SSH public keys for any additional users specified in config
    if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
        ADMIN_PASS=
        ADMIN_PUBLIC_KEY=
        let index=0
        while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
            echo ""
            ask_password "Enter password for ${CONFIG_NAME} '${ADMIN_USER[$index]}'"
            ADMIN_PASS[index]=$(encr_pwd "${_password}")
            echo ""
            ask_input "Paste public key for ${CONFIG_NAME} '${ADMIN_USER[$index]}' (or blank for none) "
            ADMIN_PUBLIC_KEY[index]="${resp}"
            let index+=1
        done
    fi

    # Read PSKs to use for replacement in IPSec config if requested
    if [[ ${#IPSEC_PSK_NAME[*]} -gt 0 ]]; then
        IPSEC_PSK=
        let index=0
        while [[ $index -lt ${#IPSEC_PSK_NAME[*]} ]]; do
            echo ""
            ask_password "Paste PSK for '${IPSEC_PSK_NAME[$index]}' (or blank for none)"
            IPSEC_PSK[index]="${_password}"
            let index+=1
        done
    fi

    # Read any additional passwords for replacement, if specified in config
    # Each entry represents a PASSWORD_NAME.X to be replaced in one or more files
    # PASSWORD_FILE[x]      = One or more pipe-delimited (|) full file paths to have password replaced in them
    # PASSWORD_PROMPT[x]    = Prompt to display when requesting the password from user
    # PASSWORD_TYPE[x]      = Password handling type, default/empty is no encryption, E = standard encrypt(1), S = smtpctl(8) encrypt
    # PASSWORD_PASS[x]      = Contains password value from user prompt
    if [[ ${#PASSWORD_PROMPT[*]} -gt 0 ]]; then
        PASSWORD_PASS=
        let index=0
        while [[ $index -lt ${#PASSWORD_PROMPT[*]} ]]; do
            echo ""
            ask_password "${PASSWORD_PROMPT[$index]}"

            # Default is un-encrypted password
            PASSWORD_PASS[index]="${_password}"
            if [[ ${#PASSWORD_TYPE[*]} -gt 0 ]]; then
                case "${PASSWORD_TYPE[$index]}" in
                    E) PASSWORD_PASS[index]=$(encr_pwd "${_password}");;
                    S) PASSWORD_PASS[index]=$(echo "${_password}" | smtpctl encrypt);;
                esac
            fi

            let index+=1
        done
    fi


    _start_time=$(date +%s)
    echo ""
    echo "${SMSEP}"


    # Get root hints once to avoid multiple retrievals
    if [[ -n "${CONFIGURE_UNBOUND}" && "${CONFIGURE_UNBOUND}" == "yes" ]]; then
        LOCAL_ROOT_HINT_PATH="/tmp/root.hints"
        printf "\nRetrieving current root hints"
        wget "${ROOT_HINTS_URL}" -O "${LOCAL_ROOT_HINT_PATH}"
    fi

    # Source resflash.sub to set the correct checksum algorithm ($ALG)
    # Disable shellcheck warning because we do not need to check resflash code
    # shellcheck source=/dev/null
    . "${RESFLASH_PATH}/resflash.sub"
    # Disable shellcheck warning because there will not be any spaces or globs in 'machine' output
    # shellcheck disable=SC2046
    set_attr_by_machine $(machine)


    # Start loop over images
    for IMAGE in "${IMAGE_PATH}"*.{fs,img}
    do
        printf "\n%s\n\nConfiguring image: %s\n" ${SMSEP} "${IMAGE}"
        if [[ -f "${IMAGE}" ]]; then
            echo "Mounting image: ${IMAGE}"
            "${RESFLASH_PATH}/mount_resflash.sh" "${IMAGE}" >/dev/null

            # Path to mounted image (used for path or to chroot when running commands)
            # Note: chroot only works because commands exist in chroot (image)
            FS_PATH=$(df -h | grep 'resflash.*/fs$' | awk -safe '{ print $6 }')

            echo "Starting '${CONFIG_NAME}' configuration on image mounted at ${FS_PATH}"


            # Add admin user group
            if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
                print_header "Admin user setup"
                ADMIN_USER_LIST=""
                let index=0
                while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
                    USER_NAME="${ADMIN_USER[$index]}"
                    PASS="${ADMIN_PASS[$index]}"
                    USER_ID="${ADMIN_ID[$index]}"
                    ADMIN_USER_LIST="${ADMIN_USER_LIST},${USER_NAME}"

                    echo "Adding group for '${USER_NAME}'"
                    ADMIN_GROUP="${USER_NAME}:*:${USER_ID}:"
                    grep "^${USER_NAME}:" "${FS_PATH}/etc/group" >/dev/null || echo "${ADMIN_GROUP}" >> "${FS_PATH}/etc/group"

                    echo "Adding user password entry for '${USER_NAME}'"
                    ADMIN_LINE="${USER_NAME}:${PASS}:${USER_ID}:${USER_ID}:staff:0:0:Local Admin:/home/${USER_NAME}:/bin/ksh"
                    grep "^${USER_NAME}:" "${FS_PATH}/etc/master.passwd" >/dev/null || echo "$ADMIN_LINE" >> "${FS_PATH}/etc/master.passwd"

                    let index+=1
                    echo ""
                done

                # Add admin users to wheel group
                echo "Adding admin users to wheel group"
                sed -i -e "s@^wheel:.:0:root\$@wheel:\*:0:root${ADMIN_USER_LIST}@" "${FS_PATH}/etc/group" 2>/dev/null

            fi

            if [[ ${#NO_PW_USER[*]} -gt 0 ]]; then
                print_header "No-password user setup"
                let index=0
                while [[ $index -lt ${#NO_PW_USER[*]} ]]; do
                    USER_NAME="${NO_PW_USER[$index]}"
                    PASS="*"
                    USER_ID="${NO_PW_USER_ID[$index]}"
                    USER_HOME="${NO_PW_USER_HOME[$index]}"
                    USER_SHELL="${NO_PW_USER_SHELL[$index]}"
                    DESC="${NO_PW_USER_DESC[$index]}"
                    if [[ -z "${DESC}" ]]; then
                        DESC="${USER_NAME}"
                    fi

                    echo "  Adding group for '${USER_NAME}'"
                    NO_PW_GROUP="${USER_NAME}:*:${USER_ID}:"
                    grep "^${USER_NAME}:" "${FS_PATH}/etc/group" >/dev/null || echo "${NO_PW_GROUP}" >> "${FS_PATH}/etc/group"

                    echo "  Adding user entry for '${USER_NAME}'"
                    NO_PW_LINE="${USER_NAME}:${PASS}:${USER_ID}:${USER_ID}::0:0:${DESC}:${USER_HOME}:${USER_SHELL}"
                    grep "^${USER_NAME}:" "${FS_PATH}/etc/master.passwd" >/dev/null || echo "$NO_PW_LINE" >> "${FS_PATH}/etc/master.passwd"

                    let index+=1
                    echo ""
                done
            fi

            print_header "root user setup"
            if [[ -n "${CHANGE_ROOT_NAME_TO_HOSTNAME}" && "${CHANGE_ROOT_NAME_TO_HOSTNAME}" == "yes" ]]; then
                ROOT_NAME=$(capitalize "$(echo "${MY_NAME}" | cut -d "." -f1)")
                echo "  Changing 'root' name to '${ROOT_NAME}'"
                sed -i -e "s@Charlie@${ROOT_NAME}@" "${FS_PATH}/etc/master.passwd" 2>/dev/null
            fi

            echo "  Setting man to not clear screen after exiting for 'root'"
            cat << EOF | chroot "${FS_PATH}" 2>/dev/null
echo "export MANPAGER=\"less -X\"" >> "/root/.profile"
chown root:wheel /root/.profile
EOF

            echo "  Setting vi tab stops for 'root'"
            cat << EOF | chroot "${FS_PATH}" 2>/dev/null
echo ":set expandtab\n:set shiftwidth=4\n:set tabstop=4" > /root/.nexrc
chown root:wheel /root/.nexrc
chmod go-wx /root/.nexrc
EOF

            echo ""
            echo "  Setting 'root' password"
            sed -i -e "s@^root:[^:]*:@root:${ROOT_PASS}:@" "${FS_PATH}/etc/master.passwd" 2>/dev/null


            print_header "Updating password DB"
            chroot "${FS_PATH}" pwd_mkdb -p -d /etc /etc/master.passwd


            if [[ ${#NO_PW_USER[*]} -gt 0 && ${#NO_PW_USER_GROUPS[*]} -gt 0 ]]; then
                # To use usermod, this section must come after pwd_mkdb
                print_header "Updating No-password user group membership"
                let index=0
                while [[ $index -lt ${#NO_PW_USER[*]} ]]; do
                    USER_NAME="${NO_PW_USER[$index]}"
                    local _user_groups="${NO_PW_USER_GROUPS[$index]}"
                    if [[ -n "${_user_groups}" ]]; then
                        # This will need to be chrooted in config_image
                        echo "  Assigning secondary group(s) for '${USER_NAME}'"
                        chroot "${FS_PATH}" /usr/sbin/usermod -G "${_user_groups}" "${USER_NAME}"
                        echo ""
                    fi

                    let index+=1
                done
            fi

            if [[ ${#ADMIN_USER[*]} -gt 0 ]]; then
                print_header "Setup admin users"

                let index=0
                while [[ $index -lt ${#ADMIN_USER[*]} ]]; do
                    USER_NAME="${ADMIN_USER[$index]}"
                    USER_ID="${ADMIN_ID[$index]}"

                    echo "Adding home dir for '${USER_NAME}'"
                    cat << EOF | chroot "${FS_PATH}" 2>/dev/null
mkdir -p /home/${USER_NAME}
chown ${USER_ID}:${USER_ID} /home/${USER_NAME}
EOF

                    echo "Setting man to not clear screen after exiting for '${USER_NAME}'"
                    cp "${FS_PATH}/etc/skel/.profile" "${FS_PATH}/home/${USER_NAME}/.profile"
                    cat << EOF | chroot "${FS_PATH}" 2>/dev/null
echo "export MANPAGER=\"less -X\"" >> "/home/${USER_NAME}/.profile"
chown ${USER_ID}:${USER_ID} /home/${USER_NAME}/.profile
EOF

                    echo "Setting vi tab stops for '${USER_NAME}'"
                    cat << EOF | chroot "${FS_PATH}" 2>/dev/null
echo ":set expandtab\n:set shiftwidth=4\n:set tabstop=4" > /home/${USER_NAME}/.nexrc
chown ${USER_ID}:${USER_ID} /home/${USER_NAME}/.nexrc
chmod go-wx /home/${USER_NAME}/.nexrc
EOF

                    if [[ ${#ADMIN_PUBLIC_KEY[*]} -gt 0 ]]; then
                        KEY="${ADMIN_PUBLIC_KEY[$index]}"
                        echo "Adding public key for '${USER_NAME}'"
                        cat << EOF | chroot "${FS_PATH}" 2>/dev/null
mkdir -p /home/${USER_NAME}/.ssh
touch /home/${USER_NAME}/.ssh/authorized_keys
echo "${KEY}" >> /home/${USER_NAME}/.ssh/authorized_keys

chown -R ${USER_ID}:${USER_ID} /home/${USER_NAME}/.ssh
chmod go-rwx /home/${USER_NAME}/.ssh
chmod go-rwx /home/${USER_NAME}/.ssh/authorized_keys
EOF

                    else
                        echo "Skipping '${USER_NAME}' public key"
                    fi
                    let index+=1
                    echo ""
                done
            fi

            echo ""
            echo "${SMSEP}"

            print_header "Doas setup"
            if [[ -f "${FILES_PATH}/doas.conf" ]]; then
                echo "Copying custom doas.conf file"
                cp "${FILES_PATH}/doas.conf" "${FS_PATH}/etc/doas.conf"
            else
                echo "Copying example doas.conf file"
                cp "${FS_PATH}/etc/examples/doas.conf" "${FS_PATH}/etc/doas.conf"
                sed -i -e "s@^permit keepenv :wheel\$@permit persist keepenv :wheel@" "${FS_PATH}/etc/doas.conf" 2>/dev/null
            fi
            chmod 0600 "${FS_PATH}/etc/doas.conf"

            if [[ -n "${ADD_RO_RW_SCRIPTS}" && "${ADD_RO_RW_SCRIPTS}" == "yes" ]]; then
                print_header "Adding /bin/ro and /bin/rw scripts"
                printf "#!/bin/sh\n\nmount -ur /\n" > "${FS_PATH}/bin/ro"
                printf "#!/bin/sh\n\nmount -uw /\n" > "${FS_PATH}/bin/rw"

                cat << EOF | chroot "${FS_PATH}" 2>/dev/null
chmod a+x /bin/ro
chmod a+x /bin/rw
EOF
                if [[ -n "${ADD_RO_RW_TO_WEEKLY}" && "${ADD_RO_RW_TO_WEEKLY}" == "yes" ]]; then
                    echo "  Modifying weekly with /bin/ro and /bin/rw scripts"
                    # Variables should not be expanded in sed command, disable shellcheck warning
                    # shellcheck disable=SC2016
                    sed -i -e 's@^/usr/sbin/makewhatis \$MAKEWHATISARGS$@/bin/rw; /usr/sbin/makewhatis $MAKEWHATISARGS; /bin/ro@' "${FS_PATH}/etc/weekly" 2>/dev/null
                fi
            fi

            print_header "SSH setup"
            if [[ -n "${SSH_BANNER}" ]]; then
                echo "  Adding SSH banner content to ssh_banner"
                echo "${SSH_BANNER}" >> "${FS_PATH}/etc/ssh/ssh_banner"
            else
                if [[ -f "${FILES_PATH}/ssh_banner" ]]; then
                    echo "  Copying custom ssh_banner file"
                    cp "${FILES_PATH}/ssh_banner" "${FS_PATH}/etc/ssh/ssh_banner"
                fi
            fi

            if [[ -n "${SSHD_CONFIG}" ]]; then
                echo "  Addng SSH config content to sshd_config"
                echo "${SSHD_CONFIG}" >> "${FS_PATH}/etc/ssh/sshd_config"
            else
                if [[ -f "${FILES_PATH}/sshd_config" ]]; then
                    echo "  Copying custom sshd_config file"
                    cp "${FILES_PATH}/sshd_config" "${FS_PATH}/etc/ssh/sshd_config"
                fi
            fi

            print_header "Network setup"
            if [[ -n "${MY_GATE}" ]]; then
                echo "  Adding gateway"
                echo "${MY_GATE}" > "${FS_PATH}/etc/mygate"
            fi

            if [[ -n "${MY_NAME}" ]]; then
                echo "  Adding myname"
                echo "${MY_NAME}" > "${FS_PATH}/etc/myname"
            fi

            if [[ ${#INTERFACE[*]} -gt 0 ]]; then
                # This does not need to be reversed because these are file names
                let index=0
                while [[ $index -lt ${#INTERFACE[*]} ]]; do
                    copy_file "${FILES_PATH}/hostname.${index}" "${FS_PATH}/etc/hostname.${INTERFACE[$index]}" "0640" "root:wheel"
                    let index+=1
                done
            else
                for HOSTNAME_FILE in "${FILES_PATH}/hostname."*
                do
                    DST="${FS_PATH}/etc/${HOSTNAME_FILE##*/}"
                    echo "  Copying ${HOSTNAME_FILE##*/}"
                    cp "${HOSTNAME_FILE}" "${DST}"
                    chmod 0640 "${DST}"
                done
            fi


            if [[ -f "${FILES_PATH}/dhcpd.conf" ]]; then
                echo "  Copying dhcpd.conf"
                cp "${FILES_PATH}/dhcpd.conf" "${FS_PATH}/etc/dhcpd.conf"
            fi


            if [[ -f "${FILES_PATH}/hosts" ]]; then
                echo "  Copying hosts"
                cp "${FILES_PATH}/hosts" "${FS_PATH}/etc/hosts"
            else
                if [[ ! -f "${FS_PATH}/etc/hosts" ]]; then
                    echo "  Creating default hosts file"
                    printf "127.0.0.1       localhost\n::1             localhost\n" >> "${FS_PATH}/etc/hosts"
                fi
            fi


            if [[ -n "${ISAKMPD_POLICY}" ]]; then
                echo "  Adding isakmpd policy"
                echo "${ISAKMPD_POLICY}" >> "${FS_PATH}/etc/isakmpd/isakmpd.policy"
                chmod 0600 "${FS_PATH}/etc/isakmpd/isakmpd.policy"
            else
                if [[ -f "${FILES_PATH}/isakmpd.policy" ]]; then
                    echo "  Copying isakmpd.policy"
                    cp "${FILES_PATH}/isakmpd.policy" "${FS_PATH}/etc/isakmpd/isakmpd.policy"
                    chmod 0600 "${FS_PATH}/etc/isakmpd/isakmpd.policy"
                fi
            fi


            if [[ -n "${IPSEC_FILE_PATTERN}" ]]; then
                for IPSEC_CONFIG in "${FILES_PATH}"/${IPSEC_FILE_PATTERN}
                do
                    echo "Copying '${IPSEC_CONFIG##*/}'"
                    DST="${FS_PATH}/etc/${IPSEC_CONFIG##*/}"
                    cp "${IPSEC_CONFIG}" "${DST}"
                    replace_psk_place_holders "${DST}"
                    chmod 0600 "${FS_PATH}/etc/${IPSEC_CONFIG##*/}"
                done
            fi


            if [[ -f "${FILES_PATH}/pf.conf" ]]; then
                copy_file "${FILES_PATH}/pf.conf" "${FS_PATH}/etc/pf.conf" "0600" "root:wheel"
            fi


            if [[ -n "${NTP_CONF}" ]]; then
                echo "  Adding custom NTP config to ntpd.conf"
                echo "${NTP_CONF}" >> "${FS_PATH}/etc/ntpd.conf"
            else
                if [[ -f "${FILES_PATH}/ntpd.conf" ]]; then
                    echo "  Copying custom ntpd.conf file"
                    cp "${FILES_PATH}/ntpd.conf" "${FS_PATH}/etc/ntpd.conf"
                fi
            fi


            print_header "rc.conf.local setup"
            if [[ -n "${RC_CONF_LOCAL}" ]]; then
                echo "  Adding config to rc.conf.local"
                echo "${RC_CONF_LOCAL}" >> "${FS_PATH}/etc/rc.conf.local"
                replace_interface "${FS_PATH}/etc/rc.conf.local"
            else
                if [[ -f "${FILES_PATH}/rc.conf.local" ]]; then
                    echo "  Copying custom rc.conf.local file"
                    cp "${FILES_PATH}/rc.conf.local" "${FS_PATH}/etc/rc.conf.local"
                    replace_interface "${FS_PATH}/etc/rc.conf.local"
                else
                    echo "  No rc.conf.local setup required"
                fi
            fi


            print_header "rc.local setup"
            if [[ -n "${RC_LOCAL}" ]]; then
                echo "  Adding config to rc.local"
                echo "${RC_LOCAL}" >> "${FS_PATH}/etc/rc.local"
                replace_interface "${FS_PATH}/etc/rc.local"
            else
                if [[ -f "${FILES_PATH}/rc.local" ]]; then
                    echo "  Copying custom rc.local file"
                    cp "${FILES_PATH}/rc.local" "${FS_PATH}/etc/rc.local"
                    replace_interface "${FS_PATH}/etc/rc.local"
                else
                    echo "  No rc.local setup required"
                fi
            fi


            print_header "sysctl.conf setup"
            if [[ -f "${FILES_PATH}/sysctl.conf" ]]; then
                echo "  Copying custom sysctl.conf file"
                cp "${FILES_PATH}/sysctl.conf" "${FS_PATH}/etc/sysctl.conf"
            else
                echo "  Copying default sysctl.conf"
                cp "${FS_PATH}/etc/examples/sysctl.conf" "${FS_PATH}/etc/sysctl.conf"
            fi

            if [[ -n "${SYSCTL_ENABLE_IPV4_FORWARDING}" && "${SYSCTL_ENABLE_IPV4_FORWARDING}" == "yes" ]]; then
                echo "Enabling 'net.inet.ip.forwarding' in sysctl.conf"
                sed -i -e "s@^#net.inet.ip.forwarding=[01]@net.inet.ip.forwarding=1@" "${FS_PATH}/etc/sysctl.conf" 2>/dev/null
            else
                echo "Disabling 'net.inet.ip.forwarding' in sysctl.conf"
                sed -i -e "s@^#net.inet.ip.forwarding=[01]@net.inet.ip.forwarding=0@" "${FS_PATH}/etc/sysctl.conf" 2>/dev/null
            fi

            if [[ -n "${SYSCTL_ENABLE_IPV6_FORWARDING}" && "${SYSCTL_ENABLE_IPV6_FORWARDING}" == "yes" ]]; then
                echo "Enabling 'net.inet6.ip6.forwarding' in sysctl.conf"
                sed -i -e "s@^#net.inet6.ip6.forwarding=[01]@net.inet6.ip6.forwarding=1@" "${FS_PATH}/etc/sysctl.conf" 2>/dev/null
            else
                echo "Disabling 'net.inet6.ip6.forwarding' in sysctl.conf"
                sed -i -e "s@^#net.inet6.ip6.forwarding=[01]@net.inet6.ip6.forwarding=0@" "${FS_PATH}/etc/sysctl.conf" 2>/dev/null
            fi


            if [[ -n "${CONFIGURE_UNBOUND}" && "${CONFIGURE_UNBOUND}" == "yes" ]]; then
                print_header "Configuring Unbound"
                if [[ -z "${UNBOUND_CONF}" ]]; then
                    # Set default file name if none specified
                    UNBOUND_CONF="unbound.conf"
                fi

                if [[ -f "${FILES_PATH}/${UNBOUND_CONF}" ]]; then
                    echo "Copying '${UNBOUND_CONF}' to default unbound config path: '/var/unbound/etc/unbound.conf'"
                    DST="${FS_PATH}/var/unbound/etc/unbound.conf"
                    cp "${FILES_PATH}/${UNBOUND_CONF}" "${DST}"
                    replace_interface "${DST}"
                else
                    echo "Using default 'unbound.conf'. This may NOT be what you want."
                fi

                if [[ -f "${LOCAL_ROOT_HINT_PATH}" ]]; then
                    echo "Copying root hints"
                    cp "${LOCAL_ROOT_HINT_PATH}" "${FS_PATH}/var/unbound/etc/root.hints"
                else
                    # Should not be needed, but just in case
                    echo "Retrieving current root hints"
                    wget "${ROOT_HINTS_URL}" -O "${FS_PATH}/var/unbound/etc/root.hints"
                fi

                echo "Running 'unbound-anchor' to update the root trust anchor for DNSSEC validation"
                unbound-anchor -a "${FS_PATH}/var/unbound/db/root.key" -r "${FS_PATH}/var/unbound/etc/root.hints"
            fi

            echo ""
            echo "${SMSEP}"

            print_header "Additional files"
            copy_files "${FS_PATH}/etc" "${MISC_ETC_FILE[*]}" "${MISC_ETC_PERMS[*]}" "${MISC_ETC_OWNER[*]}"
            copy_files "${FS_PATH}" "${MISC_MISC_FILE[*]}" "${MISC_MISC_PERMS[*]}" "${MISC_MISC_OWNER[*]}"
            if [[ ${#SCRIPT_FILE[*]} -gt 0 && -n "${SCRIPT_PATH}" ]]; then
                print_header "Additional scripts"
                if ! [[ -d "${SCRIPT_PATH}" ]] ; then
                    echo "Creating script path: ${SCRIPT_PATH}"
                    mkdir -p "${SCRIPT_PATH}"
                fi

                echo "${SMSEP}"
                copy_files "${FS_PATH}/${SCRIPT_PATH}" "${SCRIPT_FILE[*]}" "${SCRIPT_PERMS[*]}" "${SCRIPT_OWNER[*]}"
            fi
            echo "${SMSEP}"

            if [[ -n "${ADDL_CRON_PATH}" && -f "${FILES_PATH}/${ADDL_CRON_PATH}" ]]; then
                print_header "Adding root crontab entries from '${ADDL_CRON_PATH}'"
                # -r is not needed to read crontab lines, disable shellcheck warning
                # shellcheck disable=SC2162
                while read CRONSPEC
                do
                    echo "Adding cron line: '${CRONSPEC}'"
                    cat << EOF | chroot "${FS_PATH}" 2>/dev/null
{ crontab -l -u root; echo "${CRONSPEC}"; } | crontab -u root -
EOF
                done < "${FILES_PATH}/${ADDL_CRON_PATH}"
            fi

            if [[ ${#PASSWORD_FILE[*]} -gt 0 && ${#PASSWORD_PASS[*]} -gt 0 ]]; then
                print_header "Replacing passwords"
                replace_all_password_place_holders
            fi

            if [[ -n "${TIMEZONE_PATH}" ]]; then
                print_header "Timezone setup"
                echo "Setting timezone to '${TIMEZONE_PATH}'"
                chroot "${FS_PATH}" ln -fs "/usr/share/zoneinfo/${TIMEZONE_PATH}" "/etc/localtime"
            fi

            printf "\nFinished configuring '%s' on image mounted at %s\n" "${CONFIG_NAME}" "${FS_PATH}"
            echo "Unmounting ${IMAGE}"
            "${RESFLASH_PATH}/umount_resflash.sh"

            echo "Generating checksum for modified image"
            if [[ -f "${IMAGE}.cksum" ]]; then
                rm -f "${IMAGE}.cksum"
            fi
            cksum -a "${ALG}" -h "${IMAGE}.cksum" "${IMAGE}"
            if [ -n "${XXHALG+1}" ]; then
              # Pass -q to xxhsum to prevent it from clearing the line
              # Code from resflash implementation, disable shellcheck warning
              # shellcheck disable=SC2086
              xxhsum -H${XXHALG} -q "${IMAGE}" > "${IMAGE}.xxhsum"
            fi

            echo "Completed configuration of image: ${IMAGE}"

        else
            printf "\nCannot find image file: %s\n" "${IMAGE}"
            printf "Image file must exist.\n\n"

        fi
    done

    if [[ -n "${CONFIGURE_UNBOUND}" && "${CONFIGURE_UNBOUND}" == "yes" ]]; then
        if [[ -f "${LOCAL_ROOT_HINT_PATH}" ]]; then
            # Clean up root hints file to avoid any issues
            rm -rf "${LOCAL_ROOT_HINT_PATH}"
        fi
    fi

    # Post-install scripts are ignored in images

    # End recording time
    printf "\n\n%s\nImage configuration completed with config_image.sh\n Elapsed time: %ds\n%s\n" ${SMSEP} $(($(date +%s) - _start_time)) ${SEP}
else
    printf "\nPath to image is required\n"
    usage
fi
