#!/bin/sh
# Download config definitions from source control
#
# Usage:
#   /imaging/scripts/download_config.sh [-c config_archive_path [-g|--gitlab_token]]
#       -c config_archive_path              Path to the config files, local path or remote URL, assumes /tmp/config_*.tar.gz if not specified
#       -g|--gitlab_token                   Optional switch to force use of GitLab token when retrieving config files (token will be requested on console)
#
#   Example:
#       /imaging/scripts/download_config.sh -g -c https://gitlab.com/api/v4/projects/<project_id>/repository/archive.tar.gz
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
CONFIG_DIR_NAME="config"
CONFIG_FILE_PATTERN="/tmp/config_*.tar.gz"
OLD_PATH_SUFFIX=".old"
URL_STARTS_WITH_PATTERN="http*"


#####################################################################
# Password methods borrowed from OpenBSD miniroot
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
ask_pass() {
    stty -echo
    IFS= read -r resp?"$1 "
    stty echo
    echo
}

#####################################################################
# Extract at folder from a .tar.gz archive to a specific location
# Remove archive file to clean up
# $1 = archive file name
# $2 = folder to extract from archive
# $3 = destination of folder extracted from archive
# Extracted to /tmp before being moved to destination
# Returns nothing
extract_archive() {
    local _archive_name=$1
    local _folder_name=$2
    local _destination_folder=$3
    local _EXTRACT_PATH="/tmp"
    local ARCHIVE_PATH=
    local ARCHIVE_PATH_PREFIX=

    # Find folder in archive paths (won't work if there are multiple sub-folders
    # with _folder_name, assume FIRST is the one we are looking for
    if [[ -n "${_folder_name}" ]]; then
        ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | grep ${_folder_name}$ | head -n 1)
        ARCHIVE_PATH_PREFIX="${_EXTRACT_PATH}/${ARCHIVE_PATH%%/${_folder_name}}"
    else
        ARCHIVE_PATH=$(tar -ztf "${_archive_name}" | head -n 1)
    fi

    # Extract archive into temporary location
    echo "Extracting ${_folder_name} archive: ${_archive_name}"
    cd "${_EXTRACT_PATH}"
    tar -zxf "${_archive_name}"

    # Move extracted files to correct location
    echo "Moving ${ARCHIVE_PATH} to ${_destination_folder}"
    mv "${ARCHIVE_PATH}" "${_destination_folder}"

    # Clean up archive
    rm -rf "${_archive_name}"
    if [[ -n "${ARCHIVE_PATH_PREFIX}" ]]; then
        rm -rf "${ARCHIVE_PATH_PREFIX}"
    fi
}


#####################################################################
# Search for an archive first locally, and then remotely (URL)
# Removes downloaded archive file after extraction
# $1 = local file pattern
# $2 = Remote URL used to retrieve file if no local file is found
# $3 = folder to extract from archive
# $4 = destination of folder extracted from archive
# $5 = GitLab token to use, if any
# Returns nothing
get_archive() {
    local _local_pattern=$1
    local _remote_url=$2
    local _folder_name=$3
    local _destination_folder=$4
    local _gitlab_token=$5
    
    local _EXTRACT_PATH="/tmp"
    local _ARCHIVE_NAME="archive.tar.gz"
    local _ARCHIVE_PATH="${_EXTRACT_PATH}/${_ARCHIVE_NAME}"
    
    local NO_LOCAL_ARCHIVE_FOUND=true
    if [[ -n "${_local_pattern}" ]]; then
        for ARCHIVE_FILE in "${_local_pattern}"
        do
            # If file(s) don't exist only one file is returned and name = pattern
            # If pattern is a file name and not a pattern, the file will exist
            if [[ -f "${ARCHIVE_FILE}" ]]; then
                extract_archive "${ARCHIVE_FILE}" "${_folder_name}" "${_destination_folder}"
                NO_LOCAL_ARCHIVE_FOUND=false
            fi
        done
    fi

    if ${NO_LOCAL_ARCHIVE_FOUND}; then
        if [[ -n "${_remote_url}" ]]; then
            # Download scripts archive and extract
            echo "Downloading ${_folder_name} archive"
            cd "${_EXTRACT_PATH}"
            if [[ -z "${_gitlab_token}" ]]; then
                wget "${_remote_url}" -O "${_ARCHIVE_PATH}"
            else
                wget --header="PRIVATE-TOKEN: ${_gitlab_token}" "${_remote_url}" -O "${_ARCHIVE_PATH}"
            fi

            extract_archive "${_ARCHIVE_PATH}" "${_folder_name}" "${_destination_folder}"
        fi
    fi
}


SEP="=============================================================================="
SMSEP="-----------------------------------------------------------------"

EXIT_STATUS=0
ERROR_MESSAGES=
let ERROR_COUNT=0
BUILD_RELEASE=false
USE_GITLAB_TOKEN=false
CONFIG_PATH="${CONFIG_FILE_PATTERN}"
GITLAB_TOKEN=
while [[ $# -gt 0 ]]
do
    case "$1" in
        -c) CONFIG_PATH="$2"; shift;;
        -g) USE_GITLAB_TOKEN=true;;
        --gitlab_token) USE_GITLAB_TOKEN=true;;
        -*) echo >&2 \
            "Usage: $0 [-c config_archive_path [-g|--gitlab_token]]\n    -c config_archive_path  Path to the config files, local path or remote URL (default: ${CONFIG_FILE_PATTERN})\n    -g|--gitlab_token        Optional switch to force use of GitLab token when retrieving config files (token will be requested on console)"
            exit 1;;
        *)  break;;     # terminate while loop
    esac
    shift
done


echo "\n${SEP}\nStarting ${0}"
# Get token up-front
if ${USE_GITLAB_TOKEN}; then
    ask_pass "Enter GitLab token for config files archive: '${CONFIG_PATH}':"
    GITLAB_TOKEN="${resp}"
fi

# Install wget if needed
/usr/sbin/pkg_info -e net/wget
if [[ ${?} -ne 0 ]]; then
    echo "\n${SMSEP}\nInstalling wget"
    /usr/sbin/pkg_add -v wget
fi

echo "\n${SMSEP}\nCleaning up existing config directory"
if [[ -d "${ROOT_DIR}/${CONFIG_DIR_NAME}${OLD_PATH_SUFFIX}" ]]; then
    echo "Removing old config directory: ${ROOT_DIR}/${CONFIG_DIR_NAME}${OLD_PATH_SUFFIX}"
    rm -rf "${ROOT_DIR}/${CONFIG_DIR_NAME}${OLD_PATH_SUFFIX}"
fi

if [[ -d "${ROOT_DIR}/${CONFIG_DIR_NAME}" ]]; then
    echo "Renaming current config directory: ${ROOT_DIR}/${CONFIG_DIR_NAME} to ${ROOT_DIR}/${CONFIG_DIR_NAME}${OLD_PATH_SUFFIX}"
    mv "${ROOT_DIR}/${CONFIG_DIR_NAME}" "${ROOT_DIR}/${CONFIG_DIR_NAME}${OLD_PATH_SUFFIX}"
fi

echo "\n${SMSEP}\nUpdating config archive from source control"
case "${CONFIG_PATH}" in
    ${URL_STARTS_WITH_PATTERN})
        get_archive "" "${CONFIG_PATH}" "${CONFIG_DIR_NAME}" "${ROOT_DIR}" "${GITLAB_TOKEN}";;
    
    *)
        get_archive "${CONFIG_PATH}" "" "${CONFIG_DIR_NAME}" "${ROOT_DIR}" "${GITLAB_TOKEN}";;
esac
echo "\nEnding ${0}\n${SEP}"
