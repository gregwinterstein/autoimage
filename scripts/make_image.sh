#!/bin/sh
#
# Build and configure an image using build_premade_auto
# Simplifies setting up correct paths and starting the automated image creation process
# Displays file system image hash if successful
#
# Options:
# -b/-build_vga (default is to not build VGA images)
# 
# usage: /imaging/scripts/make_image.sh <config_name> [-b|--build_vga]
#
# Assumptions:
#   Default path to resflash is unchanged: /imaging/resflash
#   Default path to images is unchanged: /imaging/images
#   Script path is unchanged: /imaging/scripts
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
RESFLASHBIN="${ROOT_DIR}/resflash"
IMAGES_ROOT="${ROOT_DIR}/images"
PREMADE_PATH="premade/upgrade"
BUILD_PREMADE_CMD="../../scripts/build_premade_auto.sh"
EXIT_CODE=0

CONFIG_NAME=
OPTIONS=
let PARAM_COUNT=0
while [ $# -gt 0 ]
do
    case "$1" in
        -b) OPTIONS=" $1" ;;
        --build_vga) OPTIONS=" $1" ;;
        -*) echo >&2 "Usage: ${0} config_name [-b|--build_vga]"
            exit 1;;
        *) let PARAM_COUNT+=1               # Handle positional parameters
            case ${PARAM_COUNT} in
                1) CONFIG_NAME=${1};;
                *) ;;
            esac
    esac
    shift
done

if [[ -z "${CONFIG_NAME}" ]]; then
    echo >&2 "Usage: ${0} config_name [-b|--build_vga]"
    exit 1
fi

ORIG_PATH=$(pwd)

mkdir -p "${IMAGES_ROOT}/${CONFIG_NAME}"
cd "${IMAGES_ROOT}/${CONFIG_NAME}" || exit 1
"${BUILD_PREMADE_CMD}" "${RESFLASHBIN}" "${CONFIG_NAME}${OPTIONS}"


if [[ -d "${IMAGES_ROOT}/${CONFIG_NAME}/${PREMADE_PATH}" ]] ; then
    echo ""
    cat "${IMAGES_ROOT}"/"${CONFIG_NAME}"/"${PREMADE_PATH}"/*.fs.cksum
else
    EXIT_CODE=1
fi

cd ${ORIG_PATH}
exit $EXIT_CODE