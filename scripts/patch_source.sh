#!/bin/sh
#
# Automate installation of OpenBSD source and patches
#   - Install wget (used to download source and patches)
#   - Fetch source archives (src.tar.gz, sys.tar.gz, & SHA256.sig) if not found in /tmp
#   - Verifies sources if SHA256.sig found (existing or downloaded)
#   - Deletes existing source (/usr/src/*) if any
#   - Extract source into /usr/src
#   - Apply local patches, if any found in config-specific patch folder
#   - Fetch source patches
#   - Extract source patches
#   - Apply source patches
#
# Assumes source is stored in: /usr/src
# Assumes config root is: /imaging/config
# Assumes patches, if any, are in: /imaging/config/<config_name>/patches
#   and are named *.patch
#
# usage: /imaging/scripts/patch_source.sh [<config_name>]
#   config_name = Optional name of configuration (name of directory containing configuration)
#                 Used to search for config-specific source patches to apply
#
# Used in config_machine.sh for initial installation of source and patching
# Can be used stand-alone to patch before building new release
#   Example: /imaging/scripts/patch_source.sh && /imaging/scripts/build_release.sh
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
DEFAULT_SOURCE_PATH="/usr/src"
CONFIG_ROOT="${ROOT_DIR}/config"
DEFAULT_INSTALL_URL="https://cloudflare.cdn.openbsd.org/pub/OpenBSD"
PATCH_FILE_PATTERN="*_*.patch.sig"
PATCH_DIR_NAME="patches"

SEP="=============================================================================="
SMSEP="-----------------------------------------------------------------"

# Print a header using SMSEP
# $1 = header message
print_header() {
    printf "\n%s\n%s\n" ${SMSEP} "$1"
}

usage() {
    echo >&2 "Usage: ${0} [config_name]"
    exit 1
}


CONFIG_NAME=
let PARAM_COUNT=0
while [ $# -gt 0 ]
do
    case "$1" in
        -*) usage;;
        *) let PARAM_COUNT+=1                   # Handle positional parameters
            case ${PARAM_COUNT} in
                1) CONFIG_NAME=${1};;
                *) ;;
            esac
    esac
    shift
done

printf "\n%s\nStarting %s" "${SEP}" "${0}"
printf "\nGetting /etc/installurl"
if [[ -f "/etc/installurl" ]]; then
    INSTALL_URL=$(</etc/installurl)
else
    INSTALL_URL="${DEFAULT_INSTALL_URL}"
fi
echo "Using /etc/installurl = ${INSTALL_URL}"

if [[ -z $(/usr/sbin/pkg_info wget) ]];then
    print_header "Installing wget"
    /usr/sbin/pkg_add -v wget
fi

print_header "Fetching source archives"
cd /tmp
if [[ -f "/tmp/src.tar.gz" ]]; then
    echo "Found src.tar.gz. Not downloading"
else
    wget "${INSTALL_URL}/$(uname -r)/src.tar.gz"
fi

if [[ -f "/tmp/sys.tar.gz" ]]; then
    echo "Found sys.tar.gz. Not downloading"
else
    wget "${INSTALL_URL}/$(uname -r)/sys.tar.gz"
fi

# Verify source
if [[ -f "/tmp/SHA256.sig" ]]; then
    echo "Found SHA256.sig. Not downloading"
else
    echo "Downloading SHA256.sig."
    wget "${INSTALL_URL}/$(uname -r)/SHA256.sig"
fi
if [[ -f "/tmp/SHA256.sig" ]]; then
    printf "\n%s\nVerifying source\n" "${SMSEP}"
    # signify fails if files to be verified have paths associated with them, change to /tmp
    cd /tmp
    OS_VER=$(uname -r | tr -d '.')
    if signify -C -p "/etc/signify/openbsd-${OS_VER}-base.pub" -x SHA256.sig src.tar.gz sys.tar.gz; then
        echo "Source archives verified"
    else
        echo "Signify failed to verify sources"
        exit 1
    fi
else
    echo "SHA256.sig not found. Not verifying source."
fi

# Delete existing usr/src
if [[ -n "$(find ${DEFAULT_SOURCE_PATH}/. -name . -o -print | head -n 1)" ]]; then
    print_header "Deleting source"
    rm -rf "${DEFAULT_SOURCE_PATH:?}/*"
fi

print_header "Extracting source"
cd "${DEFAULT_SOURCE_PATH}"
tar xfz /tmp/src.tar.gz
cd "${DEFAULT_SOURCE_PATH}"
tar xfz /tmp/sys.tar.gz

# If config_name was specified, we check for config-specific patch files and attempt to apply
if [[ -n "${CONFIG_NAME}" ]]; then
    PATCH_DIR="${CONFIG_ROOT}/${CONFIG_NAME}/${PATCH_DIR_NAME}"
    if [[ -d "${PATCH_DIR}" ]]; then
        cd "${DEFAULT_SOURCE_PATH}"
        PATCH_PATTERN="${PATCH_DIR}/*.patch"
        print_header "Apply local patches"
        echo "Applying local patches for '${CONFIG_NAME}' from ${PATCH_DIR}"
        for PATCH_FILE in $PATCH_PATTERN
        do
            PATCH_CMD="patch ${PATCH_FILE}"
            if [ "${PATCH_CMD}" ]; then
                echo "${PATCH_FILE} successful"

            else
                # May need to stop here?
                # Only if we are patching something else in ${PATCH_DIR}
                echo "Failure applying ${PATCH_FILE}"
            fi
        done
    else
        echo "Patch directory not found: '${PATCH_DIR}'. Not using local patches."
    fi
fi


print_header "Cleanup previous errata patches"
if [[ -f "/tmp/$(uname -r).tar.gz" ]]; then
    echo "Removing /tmp/$(uname -r).tar.gz"
    rm -rf "/tmp/$(uname -r).tar.gz"
fi

if [[ -d "/tmp/$(uname -r)" ]]; then
    echo "Removing /tmp/$(uname -r)"
    rm -rf "/tmp/$(uname -r)"
fi

print_header "Fetching source errata patches"
cd /tmp
wget "${INSTALL_URL}/patches/$(uname -r).tar.gz"

if [[ -f "/tmp/$(uname -r).tar.gz" ]]; then
    echo "Extracting source errata patches"
    cd /tmp
    tar -zxf "$(uname -r).tar.gz"

    if [[ -d "/tmp/$(uname -r)/common" ]]; then
        cd "/tmp/$(uname -r)/common"
        for PATCH_FILE in $PATCH_FILE_PATTERN
        do
            cd "/tmp/$(uname -r)/common"

            # Get signify line(s) from each file
            # Most of the lines are in the format:
            #   signify -Vep /etc/signify/openbsd-64-base.pub -x 001_xserver.patch.sig \
            #       -m - | (cd /usr/xenocara && patch -p0)

            SIGNIFY_LINE=$(grep -Eo 'signify.*$' "${PATCH_FILE}")
            PATCH_LINE=$(grep -Eo '\-m.*patch \-p.*\).*$' "${PATCH_FILE}")

            if [[ -n $SIGNIFY_LINE && -n $PATCH_LINE ]]; then
                # Strip any trailing backslash and join lines together
                PATCH_CMD="${SIGNIFY_LINE%\\}${PATCH_LINE}"
                PATCH_DIR="$(echo "${PATCH_LINE}" | awk -safe '{ print $5 }')"

                if [[ -d "${PATCH_DIR}" ]]; then
                    print_header "Applying patch file: ${PATCH_FILE}";
                    # patch command should return 0 on success
                    if [ "${PATCH_CMD}" ]; then
                        echo "${PATCH_FILE} successful"

                    else
                        echo "Failure applying ${PATCH_FILE}"
                        exit 1
                    fi

                else
                    # Usually xenocara which is not used on routers
                    print_header "${PATCH_DIR} not found. Not applying patch ${PATCH_FILE}"
                fi

            else
                # Something's wrong and needs investigation
                printf "\n\nFailed to find patch command in patch file: %s" "${PATCH_FILE}"
                exit 1
            fi

        done

    else
        echo "Expected patch directory not found after extracting patches in /tmp/$(uname -r)/common"
        exit 1
    fi

else
    echo "No patch archive found at: ${INSTALL_URL}/patches/$(uname -r).tar.gz"
fi
printf "\nEnding %s\n%s" "${0}" "${SEP}"
