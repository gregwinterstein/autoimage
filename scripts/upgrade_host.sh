#!/bin/sh
#
# upgrade_host.sh
#
# Create an image and apply it to a target host
#  Calls make_image.sh
#  Calls upgrade_remote_host.sh passing credentials
#
# usage: /imaging/scripts/upgrade_host.sh <config_name> <host_name_or_ip> <ssh_key_path> [-b|--build_vga] [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]
#   config_name = name of configuration (name of directory containing configuration)
#   host_name_or_ip = Name (or IP address) of target host where image is to be upgraded
#   ssh_key_path = path to private SSH key to use to access the target host when upgrading the image
#
# Options:
# -k/--keep_known_hosts Don't clean known hosts (default to delete known hosts file)
# -n <ssh_key_path>/--newkey <ssh_key_path> Optional updated SSH key to use to access Target Host (leave out if key has not changed)
# -p <port>/--port <port> Target host SSH port number (defaults to default SSH port in /etc/services)
# -r/--reboot Reboot target host if image was successfully upgraded (default is to NOT reboot target host)
#
# Make Options:
# -b/--build_vga (default to to not building VGA images)
#
#
# Assumptions:
#   Default path to resflash is unchanged: /imaging/resflash
#   Default path to images is unchanged: /imaging/images
#   Script path is unchanged: /imaging/scripts
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
SCRIPT_ROOT="${ROOT_DIR}/scripts"

#####################################################################
# Functions
#####################################################################
usage() {
    echo >&2 "Usage: ${0} config_name target_host_name ssh_key_path [-b|--build_vga] [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]"
    exit 1
}

# Password methods borrowed from OpenBSD miniroot
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
#
ask_pass() {
        stty -echo
        IFS= read -r resp?"$1 "
        stty echo
        echo
}

# Ask for a password twice, saving the input in $_password.
ask_password() {
        local _q=$1

        while :; do
                ask_pass "$_q (will not echo)"
                # resp is referenced as a global variable
                # shellcheck disable=SC2154
                _password=$resp

                ask_pass "$_q (again)"
                [[ $resp == "$_password" ]] && break

                echo "Passwords do not match, try again."
        done
}

# Check and set file mode if not the expected mode (ignores non-existent files)
# $1 = path to file
# $2 = OCTAL file mode to set
set_file_mode() {
    local _file_path=$1
    local _file_mode=$2
    local FILE_MODE=

    if [[ -f "${_file_path}" ]] ; then
        FILE_MODE=$(stat -f "%#OLp" "${_file_path}")

        if [[ "${FILE_MODE}" != "${_file_mode}" ]] ; then
            chmod ${_file_mode} "${_file_path}"
        fi
    fi
}


#####################################################################
# Main script
echo "=============================================================================="
echo "Starting '${0}'"
EXIT_CODE=0


CONFIG_NAME=
TARGET_HOST=
SSH_KEY_PATH=
NEW_SSH_KEY_PATH=
TARGET_HOST_PORT=
MAKE_OPTIONS=
UPGRADE_OPTIONS=
SSH_KEY_PASS=
NEW_SSH_KEY_PASS=
let PARAM_COUNT=0
while [ $# -gt 0 ]
do
    case "$1" in
        -b) MAKE_OPTIONS=" $1" ;;
        --build_vga) MAKE_OPTIONS=" $1" ;;
        -k) UPGRADE_OPTIONS="${UPGRADE_OPTIONS} $1" ;;
        --keep_known_hosts) UPGRADE_OPTIONS="${UPGRADE_OPTIONS} $1" ;;
        -n) NEW_SSH_KEY_PATH="${2}"; shift ;;
        --newkey) NEW_SSH_KEY_PATH="${2}"; shift ;;
        -p) TARGET_HOST_PORT="${2}"; shift ;;
        --port) TARGET_HOST_PORT="${2}"; shift ;;
        -r) UPGRADE_OPTIONS="${UPGRADE_OPTIONS} $1" ;;
        --reboot) UPGRADE_OPTIONS="${UPGRADE_OPTIONS} $1" ;;
        -*) usage;;
        *) let PARAM_COUNT+=1                   # Handle positional parameters
            case ${PARAM_COUNT} in
                1) CONFIG_NAME=${1};;
                2) TARGET_HOST=${1};;
                3) SSH_KEY_PATH=${1};;
                *) ;;
            esac
    esac
    shift
done

if [[ -z "${CONFIG_NAME}" || -z "${TARGET_HOST}" || -z "${SSH_KEY_PATH}" ]]; then
    usage
fi

if [[ ! -f "${SSH_KEY_PATH}" ]]; then
    echo >&2 "SSH Key not found: '${SSH_KEY_PATH}'"
    usage
else
    # Check for 0600 permissions, change if necessary
    # SSH will fail with keys that are not 0600
    set_file_mode "${SSH_KEY_PATH}" 0600
fi

if [[ -n "${NEW_SSH_KEY_PATH}" && -f "${NEW_SSH_KEY_PATH}" ]] ; then
    # Check for 0600 permissions, change if necessary
    # SSH will fail with keys that are not 0600
    set_file_mode "${NEW_SSH_KEY_PATH}" 0600
fi


# Setup
# Get credentials & etc up-front because make_image will request credentials soon, too
# This allows the remainder of the process to run without asking the user for credentials
printf "\nEnter current remote credentials used for doas to start upgrade on remote host\n"
IFS= read -r REMOTE_USER_NAME?"Enter user name for user associated with SSH KEY: ${SSH_KEY_PATH}: "

# Get user password
ask_password "Enter user password for '${REMOTE_USER_NAME}'"
REMOTE_USER_PASS="${_password}"

# Get SSH-key passphrase
echo ""
ask_password "Enter passphrase for ${SSH_KEY_PATH}:"
SSH_KEY_PASS="${_password}"

if [[ -n "${NEW_SSH_KEY_PATH}" ]] ; then
    echo ""
    if [[ -f "${NEW_SSH_KEY_PATH}" ]] ; then
        ask_password "Enter passphrase for ${NEW_SSH_KEY_PATH}:"
        NEW_SSH_KEY_PASS="${_password}"
        UPGRADE_OPTIONS="${UPGRADE_OPTIONS} --newkey ${NEW_SSH_KEY_PATH}"
    else
        printf "Cannot find specified new ssh key (--newkey): '%s'\n" "${NEW_SSH_KEY_PATH}"
        exit 1
    fi
fi

# Call make_image
# MAKE_OPTIONS is potentially multiple options that should not be quoted into a single option, do not quote
# shellcheck disable=SC2086
if "${SCRIPT_ROOT}"/make_image.sh "${CONFIG_NAME}" ${MAKE_OPTIONS}; then
    echo "=============================================================================="
    # If make_image succeeds, call upgrade_remote_host
    if [[ -n "${TARGET_HOST_PORT}" ]] ; then
        # Add port to options being passed
        UPGRADE_OPTIONS="${UPGRADE_OPTIONS} --port ${TARGET_HOST_PORT}"
    fi
    if echo "${REMOTE_USER_NAME}\t${REMOTE_USER_PASS}\t${SSH_KEY_PASS}\t${NEW_SSH_KEY_PASS}" | "${SCRIPT_ROOT}"/upgrade_remote_host.sh "${CONFIG_NAME}" "${TARGET_HOST}" "${SSH_KEY_PATH}" ${UPGRADE_OPTIONS} ; then
        echo "=============================================================================="
    else
        # Upgrade failed
        echo "=============================================================================="
        EXIT_CODE=1
    fi
else
    # Make image failed
    echo "=============================================================================="
    echo "Failed to create image for upgrade. Exiting."
    EXIT_CODE=1
fi

echo "Ending '${0}'"
echo "=============================================================================="
exit $EXIT_CODE
