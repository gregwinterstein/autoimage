#!/bin/sh
#
# upgrade_remote_host.sh
#
# Automatically run resflash upgrade process on specified remote host
# Cleans up known_hosts on current user because we assume that the host key has changed due to the last image update
#
# usage: /imaging/scripts/upgrade_remote_host.sh config_name host_name_or_ip ssh_key_path [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]
#
# Options:
# -k/--keep_known_hosts Don't clean known hosts (default to delete known hosts file)
# -n <ssh_key_path>/--newkey <ssh_key_path> Optional updated SSH key to use to access Target Host (leave out if key has not changed)
# -p <port>/--port <port> Target host SSH port number (defaults to default SSH port in /etc/services)
# -r/--reboot Reboot target host if image was successfully upgraded (default is to NOT reboot target host)
#
# When used in automation, expects credentials on stdin. If credentials are not found on stdin
# user is prompted for credentials.
# Credentials are separated by tabs (\t)
#   REMOTE_USER_NAME\tREMOTE_USER_PASS\tSSH_KEY_PASS\tNEW_SSH_KEY_PASS
#       REMOTE_USER_NAME = user name used to login via SSH to target host
#       REMOTE_USER_PASS = Password for REMOTE_USER_NAME used to run doas commands
#       SSH_KEY_PASS = Passphrase for ssh_key_path (used to add key to ssh-agent)
#       NEW_SSH_KEY_PASS = Passphrase for new ssh_key_path (used to add key to ssh-agent)
#
# Requires: expect
#
# Assumptions:
#   Default path to resflash is unchanged: /imaging/resflash
#   Default path to images is unchanged: /imaging/images
#   Script path is unchanged: /imaging/scripts
#

ROOT_DIR="/$(/usr/bin/dirname "$(/usr/bin/realpath "${0}")" | /usr/bin/cut -d "/" -f2)"
KNOWN_HOSTS_PATH="${HOME}/.ssh/known_hosts"
IMAGES_ROOT="${ROOT_DIR}/images"
PREMADE_PATH="premade/upgrade"

#####################################################################
# Functions
#####################################################################
usage() {
    echo >&2 "Usage: ${0} config_name host_name_or_ip ssh_key_path [-k|--keep_known_hosts] [-n ssh_key_path|--newkey ssh_key_path] [-p ssh_port|--port ssh_port] [-r|--reboot]"
    exit 1
}

# Password methods borrowed from OpenBSD miniroot
# Ask for a password, saving the input in $resp.
#
# 1) Display $1 as the prompt.
# 2) *Don't* allow the '!' options that ask does.
# 3) *Don't* echo input.
# 4) *Don't* interpret "\" as escape character.
# 5) Preserve whitespace in input
#
ask_pass() {
        stty -echo
        IFS= read -r resp?"$1 "
        stty echo
        echo
}

# Ask for a password twice, saving the input in $_password.
ask_password() {
        local _q=$1

        while :; do
                ask_pass "$_q (will not echo)"
                # resp is assigned in ask_pass
                # shellcheck disable=SC2154
                _password=$resp

                ask_pass "$_q (again)"
                [[ $resp == "$_password" ]] && break

                echo "Passwords do not match, try again."
        done
}

# Remove specified host/port combination from known hosts file
# Searches combinations of host name and port in known hosts file
# $1 = Known hosts path (only used for logging)
# $2 = Target host (to search in known hosts file)
# $3 = Target host port (to search in known hosts file)
clean_known_hosts() {
    local _hosts_path="${1}"
    local _target_host="${2}"
    local _host_port="${3}"

    if [[ -f "${_hosts_path}" ]] ; then
        # shellcheck disable=SC2086
        if ssh-keygen -F ["${_target_host}"]:${_host_port} >/dev/null ; then
            ssh-keygen -R ["${_target_host}"]:${_host_port} >/dev/null
        else
            if ssh-keygen -F ["${_target_host}"] >/dev/null ; then
                ssh-keygen -R ["${_target_host}"] >/dev/null
            else
                if ssh-keygen -F "${_target_host}" >/dev/null ; then
                    ssh-keygen -R "${_target_host}" >/dev/null
                else
                    if ssh-keygen -F "${_target_host}":${_host_port} >/dev/null ; then
                        ssh-keygen -R "${_target_host}":${_host_port} >/dev/null
                    else
                        echo "${_target_host} not found in ${_hosts_path}"
                    fi
                fi
            fi
        fi
    fi
}

# Search for a file path, if found, path placed in $FILE_PATH
# If multiple files are found, returns first file found
# $1 = Directory path
# $2 = File spec to search
get_file_path() {
    local _directory_path=$1
    local _file_spec=$2
    FILE_PATH=

    # Combine path with spec, add slash if none is present
    local _full_path="${_directory_path}"
    if [ "${_directory_path#"${_directory_path%?}"}" != "/" ] ; then
        _full_path="${_full_path}/"
    fi
    _full_path="${_full_path}${_file_spec}"

    if [[ -d "${_directory_path}" ]] ; then
        for FILE in ${_full_path}
        do
            FILE_PATH="${FILE}"
            break;
        done
    fi
}

# Read the first line of a file and place in $FILE_CONTENT
# $1 = path to file to read
get_file_first_line() {
    local _file_path=$1
    FILE_CONTENT=

    if [[ -f "${_file_path}" ]] ; then
        while read -r line
        do
            FILE_CONTENT="${line}"
            break;
        done < "${_file_path}"
    fi
}

# Check and set file mode if not the expected mode (ignores non-existent files)
# $1 = path to file
# $2 = OCTAL file mode to set
set_file_mode() {
    local _file_path=$1
    local _file_mode=$2
    local FILE_MODE=

    if [[ -f "${_file_path}" ]] ; then
        FILE_MODE=$(stat -f "%#OLp" "${_file_path}")

        if [[ "${FILE_MODE}" != "${_file_mode}" ]] ; then
            chmod "${_file_mode}" "${_file_path}"
        fi
    fi
}


#####################################################################
# Main script

echo "-----------------------------------------------------------------"
echo "Starting '${0}'"

IMAGE_FILE=
REMOTE_USER_NAME=
REMOTE_USER_PASS=
NEW_SSH_KEY_PATH=
SSH_KEY_PASS=
NEW_SSH_KEY_PASS=
KEEP_KNOWN_HOSTS=false
REBOOT=false
CONFIG_NAME=
TARGET_HOST=
TARGET_HOST_PORT=$(grep ssh /etc/services | grep tcp | cut -f 3 | cut -d '/' -f 1)
SSH_KEY_PATH=
REMOTE_UPGRADED_FILE_SYSTEM=
ACTIVE_PARTITION=
REMOTE_IMAGE_HASH=
let PARAM_COUNT=0
while [ $# -gt 0 ]
do
    case "$1" in
        -k) KEEP_KNOWN_HOSTS=true;;
        --keep_known_hosts) KEEP_KNOWN_HOSTS=true;;
        -n) NEW_SSH_KEY_PATH="${2}"; shift ;;
        --newkey) NEW_SSH_KEY_PATH="${2}"; shift ;;
        -p) TARGET_HOST_PORT="${2}"; shift ;;
        --port) TARGET_HOST_PORT="${2}"; shift ;;
        -r) REBOOT=true;;
        --reboot) REBOOT=true;;
        -*) usage;;
        *) let PARAM_COUNT+=1                   # Handle positional parameters
            case ${PARAM_COUNT} in
                1) CONFIG_NAME=${1};;
                2) TARGET_HOST=${1};;
                3) SSH_KEY_PATH=${1};;
                *) ;;
            esac
    esac
    shift
done

if [[ -z "${CONFIG_NAME}" || -z "${TARGET_HOST}" || -z "${SSH_KEY_PATH}" ]] ; then
    usage
fi

if [[ ! -f "${SSH_KEY_PATH}" ]] ; then
    echo >&2 "SSH Key not found: '${SSH_KEY_PATH}'"
    usage
else
    # Check for 0600 permissions, change if necessary
    # SSH will fail with keys that are not 0600
    set_file_mode "${SSH_KEY_PATH}" 0600
fi

if [[ -n "${NEW_SSH_KEY_PATH}" && -f "${NEW_SSH_KEY_PATH}" ]] ; then
    # Check for 0600 permissions, change if necessary
    # SSH will fail with keys that are not 0600
    set_file_mode "${NEW_SSH_KEY_PATH}" 0600
fi

# Verify we can find upgrade image
get_file_path "${IMAGES_ROOT}/${CONFIG_NAME}/${PREMADE_PATH}" "*.fs.gz"
if [[ -z "${FILE_PATH}" ]] ; then
    # No image found, exiting
    echo >&2 "Upgrade image was not found in: '${IMAGES_ROOT}/${CONFIG_NAME}/${PREMADE_PATH}/'"
    usage
else
    IMAGE_FILE="${FILE_PATH}"
    # Get image hash for later comparison
    get_file_path "${IMAGES_ROOT}/${CONFIG_NAME}/${PREMADE_PATH}" "*.fs.cksum"

    if [[ -z "${FILE_PATH}" ]] ; then
        # No image hash found, exiting
        echo >&2 "Upgrade image hash was not found in: '${IMAGES_ROOT}/${CONFIG_NAME}/${PREMADE_PATH}/'"
        usage
    else
        get_file_first_line "${FILE_PATH}"
        IMAGE_HASH=$(echo "${FILE_CONTENT}" | cut -d '=' -f 2 | tr -d " ")

        if [[ -z "${IMAGE_HASH}" ]] ; then
            # No image hash extracted
            printf "No image hash extracted from: '%s'\n%s" "${FILE_PATH}" "${FILE_CONTENT}"  >&2
            usage
        fi
    fi
fi


# Set exit code. About to start ssh-agent which needs to be
# cleaned up on exit
EXIT_CODE=0

# Start SSH Agent (PID will be automatically stored in SSH_AGENT_PID)
echo "Starting ssh-agent"
eval "$(ssh-agent -s)" >/dev/null

# Read credentials from stdin (or ask user if none are available)
if [ -t 0 ] ; then
        # Interactive Mode - Ask User for credentials
        # Get user name & password for doas
        printf "\nEnter remote credentials used for doas on remote host"
        IFS= read -r REMOTE_USER_NAME?"Enter user name for user associated with SSH KEY: ${SSH_KEY_PATH}: "

        # Get user password
        ask_password "Enter user password for '${REMOTE_USER_NAME}'"
        REMOTE_USER_PASS="${_password}"

        # In interactive mode, let ssh-add ask user for passphrase
        # Load key (this will request SSH key passphrase from user)
        ssh-add -q "${SSH_KEY_PATH}"

        if [[ -n "${NEW_SSH_KEY_PATH}" ]] ; then
            ssh-add -q "${NEW_SSH_KEY_PATH}"
        fi
else
    # Non-interactive mode, slurp credentials
    IFS= read -r FILECREDS
    # Parse credentials into values separated by tabs (\t)
    REMOTE_USER_NAME=$(echo "${FILECREDS}" | cut -f 1)
    REMOTE_USER_PASS=$(echo "${FILECREDS}" | cut -f 2)
    SSH_KEY_PASS=$(echo "${FILECREDS}" | cut -f 3)
    NEW_SSH_KEY_PASS=$(echo "${FILECREDS}" | cut -f 4)

    echo "Adding SSH keys:"
    echo "  ${SSH_KEY_PATH}"
    env KEY_PASSPHRASE="${SSH_KEY_PASS}" /usr/local/bin/expect -c "log_user 0;set password \$env(KEY_PASSPHRASE);spawn -noecho ssh-add -q \"${SSH_KEY_PATH}\";expect -re \"Enter passphrase for.+:\";send \$password;send \r;expect eof"
    if [[ -n "${NEW_SSH_KEY_PATH}" ]] ; then
        echo "  ${NEW_SSH_KEY_PATH}"
        env KEY_PASSPHRASE="${NEW_SSH_KEY_PASS}" /usr/local/bin/expect -c "log_user 0;set password \$env(KEY_PASSPHRASE);spawn -noecho ssh-add -q \"${NEW_SSH_KEY_PATH}\";expect -re \"Enter passphrase for.+:\";send \$password;send \r;expect eof"
    fi
fi

VERIFY_SSH_KEY_PATH="${SSH_KEY_PATH}"
if [[ -n "${NEW_SSH_KEY_PATH}" ]] ; then
    VERIFY_SSH_KEY_PATH="${NEW_SSH_KEY_PATH}"
fi

# Clear known hosts (host keys may change on each reboot)
# Allows us to use StrictHostKeyChecking accept-new
if ! ${KEEP_KNOWN_HOSTS} ; then
    echo "Clearing known ${KNOWN_HOSTS_PATH}"
    clean_known_hosts "${KNOWN_HOSTS_PATH}" "${TARGET_HOST}" "${TARGET_HOST_PORT}"
fi

# Change doas to "nopass" so that next command succeeds without asking for password
# We use expect to handle the request for a password by doas
echo "Temporarily update doas.conf to allow commands without password for wheel users"
env USER_PASS="${REMOTE_USER_PASS}" /usr/local/bin/expect -c "log_user 0;set password \$env(USER_PASS);spawn -noecho /usr/bin/ssh -q -o \"StrictHostKeyChecking accept-new\" -tt -p ${TARGET_HOST_PORT} -i ${SSH_KEY_PATH} ${REMOTE_USER_NAME}@${TARGET_HOST} \"doas sed -i -e 's/permit persist keepenv :wheel/permit nopass keepenv :wheel/' /etc/doas.conf\";expect -re \"doas .+ password:\";send \$password;send \r;expect eof"

# Verify doas.conf was changed
# TARGET_HOST_PORT is a number, no need to quote
# shellcheck disable=SC2086
VERIFY_DOAS_CONFIG=$(ssh -q -o "StrictHostKeyChecking accept-new" -tt -p ${TARGET_HOST_PORT} -i "${SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" "doas grep wheel /etc/doas.conf | grep nopass")
if [[ -n "${VERIFY_DOAS_CONFIG}" ]]; then
    echo "doas.conf configured correctly."
    RESET_DOAS=true

    # Run upgrade
    # Depending on speed of connection it can take a while to send the unzipped image file to the remote host
    echo "-----------------------------------------------------------------"
    echo "Starting remote upgrade on ${TARGET_HOST} using '${IMAGE_FILE}'"
    # TARGET_HOST_PORT is a number, no need to quote
    # shellcheck disable=SC2086
    UPGRADE_OUTPUT=$(/usr/bin/gunzip -c "${IMAGE_FILE}" | ssh -C -p ${TARGET_HOST_PORT} -i "${SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" 'doas -n /resflash/upgrade.sh')
    EXIT_CODE=$?

    echo "-----------------------------------------------------------------"
    echo "Remote upgrade finished."
    # Parse Resflash upgrade output and attempt to extract file system and hash
    REMOTE_UPGRADED_FILE_SYSTEM=$(echo "${UPGRADE_OUTPUT}" | awk -F "(" 'NR==1{print $2; exit}' | cut -d ')' -f 1)
    REMOTE_IMAGE_HASH=$(echo "${UPGRADE_OUTPUT}" | awk 'NR==2{print $1; exit}')

    echo "Updated remote file system: ${REMOTE_UPGRADED_FILE_SYSTEM}"

    SSH_RESPONDS=false
    PING_FOUND=false

    # Evaluate upgrade response/result
    if [[ ${EXIT_CODE} -ne 0 ]] ; then
        echo "Failed to upgrade image on ${TARGET_HOST}"
        echo "Exit code: ${EXIT_CODE}"
        echo "${UPGRADE_OUTPUT}"
        echo ""
    else
        echo "Verifying upgrade"

        # Validate hash before proceeding
        if [ "${IMAGE_HASH}" = "${REMOTE_IMAGE_HASH}" ] ; then
            echo "Image has been successfully upgraded on ${TARGET_HOST}"

            # Reboot if requested
            if ${REBOOT} ; then
                # Trigger reboot remotely (we still have doas without a password)
                echo "Rebooting ${TARGET_HOST}"
                # TARGET_HOST_PORT is a number, no need to quote
                # shellcheck disable=SC2086
                if ssh -q -o "StrictHostKeyChecking accept-new" -tt -p ${TARGET_HOST_PORT} -i "${SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" "doas shutdown -r now" ; then
                    RESET_DOAS=false
                fi

                # Sleep for one minute to allow time for shutdown
                echo "-----------------------------------------------------------------"
                echo "Waiting for ${TARGET_HOST} to start responding again"
                sleep 60

                # Check for ping every 30 seconds after until we get a response from ping
                # ping returns 0 if at least one response is received otherwise non-zero
                PING_COUNT=0
                while [[ ${PING_COUNT} -le 20 ]] ; do
                    if ping -c 3 "${TARGET_HOST}" ; then
                        echo "${TARGET_HOST} is responding to ping again"
                        PING_FOUND=true
                        break;
                    else
                        sleep 30
                    fi
                    let PING_COUNT+=1
                done

                if ${PING_FOUND} ; then
                    # Check for SSH again every 15 seconds after until we get a response from SSH
                    # Timeout after ~10 minutes
                    echo "Waiting for ${TARGET_HOST} to respond to SSH again"
                    SSH_COUNT=0
                    while [[ ${SSH_COUNT} -le 20 ]] ; do
                        # sysctl should exit with 0 on success
                        # TARGET_HOST_PORT is a number, no need to quote
                        # shellcheck disable=SC2086
                        if ssh -q -o "StrictHostKeyChecking off" -p ${TARGET_HOST_PORT} -i "${VERIFY_SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" "sysctl -n kern.version 2>&1" ; then
                            SSH_RESPONDS=true
                            echo ""
                            break;
                        else
                            sleep 15;
                        fi
                        let SSH_COUNT+=1
                    done

                    if ${SSH_RESPONDS} ; then
                        echo "${TARGET_HOST} is responding to SSH again"
                    else
                        echo "WARNING: ${TARGET_HOST} is NOT responding to SSH."
                        EXIT_CODE=1
                    fi

                else
                    # Host not responding to ping
                    echo "${TARGET_HOST} is NOT responding to ping."
                    EXIT_CODE=1
                fi
            else
                echo "${TARGET_HOST} must be rebooted to complete the upgrade process."
                RESET_DOAS=false
            fi

        else
            # Hashes do not match
            echo "FAILED: File hashes do not match"
            echo "Image:  ${IMAGE_HASH}"
            echo "Remote: ${REMOTE_IMAGE_HASH}"
            printf "\nRemote upgrade output:\n"
            echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            echo "${UPGRADE_OUTPUT}"
            echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            EXIT_CODE=1
        fi
    fi
else
    # Failed to configure doas correctly
    echo "Failed to configure doas.conf."
    EXIT_CODE=1
fi


if [[ ${EXIT_CODE} -ne 0 ]] ; then
    printf "%s upgrade has FAILED.\n\n" "${TARGET_HOST}"

    if [[ -n "${REMOTE_UPGRADED_FILE_SYSTEM}" ]] ; then
        INACTIVE_PARTITION="${REMOTE_UPGRADED_FILE_SYSTEM##"${REMOTE_UPGRADED_FILE_SYSTEM%%?}"}"
        if [ "${INACTIVE_PARTITION}" == 'd' ]; then
            ACTIVE_PARTITION="e"
        else
            ACTIVE_PARTITION="d"
        fi

        echo "-----------------------------------------------------------------"
        if ${RESET_DOAS} ; then
            echo "Active partition MUST be MANUALLY set back to '${ACTIVE_PARTITION}' in /mbr/etc/boot.conf before (re-)booting."
        else
            echo "Setting active partition back to '${ACTIVE_PARTITION}' before exiting."
            # TARGET_HOST_PORT is a number, no need to quote
            # shellcheck disable=SC2086
            ssh -q -o "StrictHostKeyChecking accept-new" -tt -p ${TARGET_HOST_PORT} -i "${SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" "doas mount /mbr; doas sed -i \"/^set device hd0/s/hd0[a-p]/hd0${ACTIVE_PARTITION}/\" /mbr/etc/boot.conf; doas sync; doas umount /mbr"
        fi
    fi
else
    echo "${TARGET_HOST} has been upgraded successfully."
fi

if ${RESET_DOAS} ; then
    echo "-----------------------------------------------------------------"
    echo "Setting doas.conf back to requiring a password before exiting."
    # TARGET_HOST_PORT is a number, no need to quote
    # shellcheck disable=SC2086
    ssh -q -o "StrictHostKeyChecking accept-new" -tt -p ${TARGET_HOST_PORT} -i "${SSH_KEY_PATH}" "${REMOTE_USER_NAME}"@"${TARGET_HOST}" "doas sed -i -e 's/permit nopass keepenv :wheel/permit persist keepenv :wheel/' /etc/doas.conf"
fi


# Cleanup
if [[ -n "${SSH_AGENT_PID}" ]] ; then
    # Shutdown SSH Agent we started
    echo "Shutting down SSH Agent '${SSH_AGENT_PID}'"
    kill -TERM "${SSH_AGENT_PID}"
fi

echo "Ending '${0}'"
echo "-----------------------------------------------------------------"

exit $EXIT_CODE